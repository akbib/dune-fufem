#
# Module providing convenience methods for compile binaries with python support.
#
# Provides the following functions:
#
# add_dune_python_flags(target1 target2 ...)
#
# adds python flags to the targets for compilation and linking
#

function(add_dune_pythonlibs_flags _targets)
    if(PYTHONLIBS_FOUND)
        foreach(_target ${_targets})
            target_link_libraries(${_target} ${PYTHON_LIBRARIES})
            # target_include_directories(${_target} PRIVATE ${PYTHON_INCLUDE_DIRS})
            # target_compile_definitions(${_target} PRIVATE "-DHAVE_PYTHON")
            # target_compile_options(${_target} PRIVATE "-fno-strict-aliasing")
            set_property(TARGET ${_target} APPEND PROPERTY INCLUDE_DIRECTORIES ${PYTHON_INCLUDE_DIRS})
            set_property(TARGET ${_target} APPEND PROPERTY COMPILE_DEFINITIONS "HAVE_PYTHON")
            set_property(TARGET ${_target} APPEND PROPERTY COMPILE_OPTIONS "-fno-strict-aliasing")
        endforeach(_target ${_targets})
    endif(PYTHONLIBS_FOUND)
endfunction(add_dune_pythonlibs_flags)
