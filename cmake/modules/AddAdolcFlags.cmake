#
# Module providing convenience methods for compile binaries with Adol-C support.
#
# Provides the following functions:
#
# add_dune_adolc_flags(target1 target2 ...)
#
# adds Adol-C flags to the targets for compilation and linking
#

function(add_dune_adolc_flags _targets)
  if(ADOLC_FOUND)
    foreach(_target ${_targets})
      target_link_libraries(${_target} ${ADOLC_LIBRARIES})
      get_target_property(_props ${_target} COMPILE_FLAGS)
      string(REPLACE "_props-NOTFOUND" "" _props "${_props}")
      set_target_properties(${_target} PROPERTIES COMPILE_FLAGS
        "${_props} ${ADOLC_COMPILE_FLAGS}")
    endforeach(_target ${_targets})
  endif(ADOLC_FOUND)
endfunction(add_dune_adolc_flags)
