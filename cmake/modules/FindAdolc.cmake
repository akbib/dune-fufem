#
# Module that checks whether Adol-C is available and usable.
#
# Add the path to your locally installed library to the variable
#    CMAKE_PREFIX_PATH
#
# Sets the following variables:
#
# ADOLC_FOUND           True if ADOL-C is available and usable.
# ADOLC_INCLUDE_DIRS    Path to the ADOL-C include directories
# ADOLC_LIBRARIES       Name to the ADOL-C library.
#

find_path(ADOLC_INCLUDE_DIR
  NAMES "adolc/adouble.h"
  PATH_SUFFIXES "include"
)

find_library(ADOLC_LIBRARY
  NAMES adolc
  PATH_SUFFIXES "lib" "lib64"
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  "Adolc"
  DEFAULT_MSG
  ADOLC_INCLUDE_DIR
  ADOLC_LIBRARY
)


mark_as_advanced(ADOLC_INCLUDE_DIR ADOLC_LIBRARY ADOLC_FOUND)


if(ADOLC_FOUND)
  set(ADOLC_INCLUDE_DIRS "${ADOLC_INCLUDE_DIR}")
  set(ADOLC_LIBRARIES "${ADOLC_LIBRARY}")
  set(ADOLC_COMPILE_FLAGS "-I${ADOLC_INCLUDE_DIR}")
  set_property(GLOBAL APPEND PROPERTY ALL_PKG_FLAGS "-I${ADOLC_INCLUDE_DIR}")

  # register all related flags
  dune_register_package_flags(LIBRARIES "${ADOLC_LIBRARY}"
      INCLUDE_DIRS "${ADOLC_INCLUDE_DIR}")
endif()

set(HAVE_ADOLC ${ADOLC_FOUND})
