/* begin dune-fufem
   put the definitions for config.h specific to
   your project here. Everything above will be
   overwritten
*/

/* begin private */
/* Name of package */
#define PACKAGE "@DUNE_MOD_NAME"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "@DUNE_MAINTAINER@"

/* Define to the full name of this package. */
#define PACKAGE_NAME "@DUNE_MOD_NAME@"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "@DUNE_MOD_NAME@ @DUNE_MOD_VERSION@"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "@DUNE_MOD_NAME@"

/* Define to the home page for this package. */
#define PACKAGE_URL "@DUNE_MOD_URL@"

/* Define to the version of this package. */
#define PACKAGE_VERSION "@DUNE_MOD_VERSION@"

/* end private */

/* Define to the version of dune-fufem */
#define DUNE_FUFEM_VERSION "@DUNE_FUFEM_VERSION@"

/* Define to the major version of dune-fufem */
#define DUNE_FUFEM_VERSION_MAJOR @DUNE_FUFEM_VERSION_MAJOR@

/* Define to the minor version of dune-fufem */
#define DUNE_FUFEM_VERSION_MINOR @DUNE_FUFE;_VERSION_MINOR@

/* Define to the revision of dune-fufem */
#define DUNE_FUFEM_VERSION_REVISION @DUNE_FUFEM_VERSION_REVISION@

/* Define to 1 if adolc library is found */
#cmakedefine HAVE_ADOLC 1

/* Define to 1 if Boost serialization library is found */
#cmakedefine HAVE_BOOST_SERIALIZATION 1

/* end dune-fufem
   Everything below here will be overwritten
*/
