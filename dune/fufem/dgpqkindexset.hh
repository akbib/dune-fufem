#ifndef DG_PQK_INDEX_SET_HH
#define DG_PQK_INDEX_SET_HH

#include <vector>

#include <dune/grid/common/mcmgmapper.hh>
#include <dune/localfunctions/lagrange/pqkfactory.hh>

/** \brief An index set for DG functions of order K*/
template <class GridView,int order>
class DGPQKIndexSet {

    //! Parameter for mapper class
    template<int dim>
    struct ElementMapperLayout
    {
        bool contains (Dune::GeometryType gt)
        {
            return gt.dim() == dim;
        }
    }; 

public:

    DGPQKIndexSet(const GridView gridview) : mapper_(gridview)
    {
        setup(gridview);
    }

    void setup(const GridView & gridview) {

        typedef typename GridView::template Codim<0>::Iterator ElementIterator;
        
        mapper_.update();

        const int dim = GridView::dimension;
        
        // Cache for local finite elements of order k
        typedef typename Dune::PQkLocalFiniteElementCache<typename GridView::ctype,double, dim, order> FECache;
        FECache cache;
        
        // Get all GeometryTypes that appear in the given grid
        const std::vector<Dune::GeometryType>& geomTypes = gridview.indexSet().geomTypes(0);
        
        //For each geometry type get the number of elements of that type and the number of its local basis functions
        //to compute the size of the Offset vector
        int counter = 0;
        for (size_t i=0;i<geomTypes.size();i++){
            int locBasisFcts = cache.get(geomTypes[i]).localBasis().size();
            int numGtElements = gridview.indexSet().size(geomTypes[i]);
            counter += locBasisFcts * numGtElements;
        }
        
        
        
        elementOffsets_.resize(gridview.size(0) + 1);
        elementOffsets_[0]=0;
        
        if (geomTypes.size()==1) {
            int locBasisFcts =cache.get(geomTypes[0]).localBasis().size(); 
            for (size_t i=1;i<elementOffsets_.size();i++)
                elementOffsets_[i]= i*locBasisFcts;
        }
        else  {
            ElementIterator eIt    = gridview.template begin<0>();
            ElementIterator eEndIt = gridview.template end<0>();
            //For each element save the number of local basis functions
            for (; eIt!=eEndIt; ++eIt) 
                    elementOffsets_[mapper_.map(*eIt) + 1] = cache.get(eIt->type()).localBasis().size();
            // Accumulate
            for (size_t i=1; i<elementOffsets_.size(); i++)
                elementOffsets_[i] += elementOffsets_[i-1];
        
        }
        
    }

    int size() const {
        return elementOffsets_[elementOffsets_.size()-1];
    }

    int operator()(const typename GridView::Grid::Traits::template Codim<0>::Entity& element) const {
        return elementOffsets_[mapper_.map(element)];
    }

    std::vector<int> elementOffsets_;

    Dune::MultipleCodimMultipleGeomTypeMapper<GridView,ElementMapperLayout> mapper_;
    
    ~ DGPQKIndexSet(){}
    
};

#endif
