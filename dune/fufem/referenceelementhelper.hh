#ifndef REFERENCEELEMENET_HELPER_HH
#define REFERENCEELEMENET_HELPER_HH



#include <dune/geometry/type.hh>

#include <dune/geometry/referenceelements.hh>



/**
 * \brief Struct with reference element related helper functions
 */
template<class ctype, int dim>
struct ReferenceElementHelper
{

    /**
     * \brief Check if one subentity contains another
     *
     * Check if i-th subentity of codim iCodim contains j-th subentity
     * of codim jCodim.
     *
     * \param i      Number of subentity
     * \param iCodim Codim of subentity
     * \param j      Number of possibly contained subentity
     * \param jCodim Codim of possibly contained subentity
     */
    static bool subEntityContainsSubEntity(const typename Dune::GeometryType& gt, int i, int iCodim, int j, int jCodim)
    {
        if (jCodim < iCodim)
            return false;

        if (jCodim == iCodim)
            return (i == j);

        // This use of ReferenceElement::subEntity is OK,
        // because we do a check for all passed indices.
        auto refElement = Dune::ReferenceElements<double, dim>::general(gt);
        for (int k = 0; k < refElement.size(i, iCodim, jCodim); ++k)
            if (refElement.subEntity(i, iCodim, k, jCodim) == j)
                return true;

        return false;
    }

};

#endif
