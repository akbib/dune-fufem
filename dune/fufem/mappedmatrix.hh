// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef MAPPED_MATRIX_HH
#define MAPPED_MATRIX_HH

#include <dune/common/iteratorfacades.hh>



/**
 * \brief Iterator over sparse row of MappedMatrix
 *
 * Be careful: Since MappedMatrix is not a real container
 * the entries can only be accessed as copies. Hence the
 * exported type Reference is not a Value&.
 *
 * As a side effect 'operator->' cannot be used because
 * Dune::ForwardIteratorFacade does not allow to specify
 * a proxy class as Pointer type. Instead it tries to
 * get an address of the result of 'operator*'.
 */
template<class Map>
class MappedMatrixColIterator :
    public Dune::ForwardIteratorFacade<
        MappedMatrixColIterator<Map>,
        typename Map::block_type,
        const typename Map::block_type>

{
    typedef typename Map::Matrix Matrix;
    static const int rowFactor = Map::rowFactor;
    static const int colFactor = Map::colFactor;

public:
    typedef typename Map::block_type Value;
    typedef typename Map::block_type Reference;

    MappedMatrixColIterator(const Map& map, typename Matrix::row_type::ConstIterator&& it, int row, int virtualRow):
        map_(&map),
        it_(it),
        row_(row),
        virtualRow_(virtualRow),
        virtualCol_(0)
    {}

    void increment()
    {
        if (virtualCol_<colFactor-1)
        {
            ++virtualCol_;
        }
        else
        {
            virtualCol_ = 0;
            ++it_;
        }
    }

    bool equals(const MappedMatrixColIterator& other) const
    {
        return (it_==other.it_) and (virtualRow_==other.virtualRow_) and (virtualCol_==other.virtualCol_);
    }

    const Reference dereference() const
    {
        return map_->apply(*it_, row_, it_.index(), virtualRow_, virtualCol_);
    }

    int index() const
    {
        return it_.index()*colFactor+virtualCol_;
    }


protected:
    const Map* map_;
    typename Matrix::row_type::ConstIterator it_;
    const int row_;
    int virtualRow_;
    int virtualCol_;
};



/**
 * \brief Sparse row of MappedMatrix
 *
 * Be careful: Since MappedMatrix is not a real container
 * the rows are proxy objects returned as copies and not
 * as references.
 */
template<class Map>
class MappedMatrixRow
{
    typedef typename Map::Matrix Matrix;
    static const int rowFactor = Map::rowFactor;
    static const int colFactor = Map::colFactor;
public:
    typedef MappedMatrixColIterator<Map> ConstIterator;

    MappedMatrixRow(const Map& map, const Matrix& matrix, int row, int virtualRow):
        map_(&map),
        matrix_(&matrix),
        row_(row),
        virtualRow_(virtualRow)
    {}

    ConstIterator begin() const
    {
        return ConstIterator(*map_, (*matrix_)[row_].begin(), row_, virtualRow_);
    }

    ConstIterator end() const
    {
        return ConstIterator(*map_, (*matrix_)[row_].end(), row_, virtualRow_);
    }

protected:
    const Map* map_;
    const Matrix* matrix_;
    int row_;
    int virtualRow_;
};



/**
 * \brief A sparse matrix built by entry-wise transformation
 *
 * MappedMatrix represents a sparse matrix that is obtained
 * by transforming each entry of some given matrix. The
 * transformed entries are not stored but computed on the fly
 * when dereferencing the corresponding iterators. The given
 * map transforms each entry into rowFactor x colFactor entries
 * that may also have a different block type.
 *
 * \tparam Map Type of transfomation map
 *
 * The type Map must implement the following interface
 * \code{.cpp}
 * class MatrixMap
 * {
 * public:
 *     static const int rowFactor; // number of rows will be this times larger
 *     static const int colFactor; // number of columns will be this times larger
 * 
 *     typedef [...] Matrix; // type of matrix to be transformed
 * 
 *     typedef [...] block_type; // type of entries for transformed matrix 
 * 
 *     // entry transformation
 *     // transform (virtualRow,virtualCol)-th copy of (row,col)-th entry A original matrix 
 *     block_type apply(const typename Matrix::block_type& A, int row, int col, int virtualRow, int virtualCol) const;
 * };
 * \endcode
 *
 * Example use cases:
 * With rowFactor=colFactor=1 one can e.g. map each entry of the original matrix
 * to its local matrix norm. Conversely one can map scalar entries to a local matrices.
 * With rowFactor,colFactor>1 one can e.g. duplicate rows or columns.
 */
template<class Map>
class MappedMatrix
{
    typedef typename Map::Matrix Matrix;
    static const int rowFactor = Map::rowFactor;
    static const int colFactor = Map::colFactor;

public:
    typedef MappedMatrixRow<Map> row_type;
    typedef typename Map::block_type block_type;
    typedef MappedMatrixColIterator<Map> ConstColIterator;

    MappedMatrix(const Map& map, const Matrix& matrix):
        map_(&map),
        matrix_(&matrix)
    {}

    int N() const
    {
        return matrix_->N()*rowFactor;
    }

    int M() const
    {
        return matrix_->M()*colFactor;
    }

    const row_type operator[](int row) const
    {
        return MappedMatrixRow<Map>(*map_, *matrix_, row/rowFactor, row % rowFactor);
    }

    template<class X , class Y >
    void mv(const X &x, Y &y) const
    {
        int rows = this->N();
        y.resize(rows);
        for(int i=0; i<rows; ++i)
        {
            y[i] = 0.0;
            const row_type& Mi = (*this)[i];
            auto it = Mi.begin();
            auto end =  Mi.end();
            for(; it!=end; ++it)
                (*it).umv(x[it.index()], y[i]);
        }
    }

protected:
    const Map* map_;
    const Matrix* matrix_;
};


#endif // MAPPED_MATRIX_HH
