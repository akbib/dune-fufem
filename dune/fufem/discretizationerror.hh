#ifndef DISCRETIZATION_ERROR_HH
#define DISCRETIZATION_ERROR_HH

#include <cmath>

#include <dune/geometry/quadraturerules.hh>

#include <dune/fufem/functions/virtualgridfunction.hh>
#include <dune/fufem/quadraturerules/quadraturerulecache.hh>

template <class GridView>
class DiscretizationError
{
    enum {dim = GridView::dimension};

    typedef typename GridView::ctype ctype;

public:

    /** \brief Compute L2 error between a grid function and an arbitrary function
     */
    template <int blocksize>
    static double computeL2Error(const VirtualGridViewFunction<GridView,Dune::FieldVector<double,blocksize> >* a,
               const Dune::VirtualFunction<Dune::FieldVector<ctype,dim>, Dune::FieldVector<double,blocksize> >* b,
               QuadratureRuleKey quadKey)
    {

    // The error to be computed
    double error = 0;

    const GridView gridView = a->gridView();

    for (auto&& element : elements(gridView))
    {
        // Get quadrature formula
        quadKey.setGeometryType(element.type());
        const auto& quad = QuadratureRuleCache<double, dim>::rule(quadKey);

        for (size_t i=0; i<quad.size(); i++) {

            // Evaluate function a
            Dune::FieldVector<double,blocksize> aValue;
            a->evaluateLocal(element, quad[i].position(),aValue);

            // Evaluate function b.  If it is a grid function use that to speed up the evaluation
            Dune::FieldVector<double,blocksize> bValue;
            if (dynamic_cast<const VirtualGridViewFunction<GridView,Dune::FieldVector<double,blocksize> >*>(b))
                dynamic_cast<const VirtualGridViewFunction<GridView,Dune::FieldVector<double,blocksize> >*>(b)->evaluateLocal(element,
                                                                                                                      quad[i].position(),
                                                                                                                      bValue
                                                                                                                     );
            else
                b->evaluate(element.geometry().global(quad[i].position()), bValue);

            // integrate error
            error += (aValue - bValue) * (aValue - bValue) * quad[i].weight() * element.geometry().integrationElement(quad[i].position());

        }

    }

    return std::sqrt(error);

    }


    template <int blocksize>
    static double computeH1HalfNormDifferenceSquared(const GridView& gridView,
                                                     const VirtualDifferentiableFunction<Dune::FieldVector<ctype,dim>, Dune::FieldVector<double,blocksize> >* u,
                                                     const VirtualDifferentiableFunction<Dune::FieldVector<ctype,dim>, Dune::FieldVector<double,blocksize> >* v,
                                                     QuadratureRuleKey quadKey)
    {
    double norm = 0.0;

    for (auto&& element : elements(gridView))
    {
        quadKey.setGeometryType(element.type());
        quadKey.setOrder(0);
        const auto& quad = QuadratureRuleCache<double, dim>::rule(quadKey);

        for (size_t ip=0; ip<quad.size(); ++ip)
        {
            // Local position of the quadrature point
            const Dune::FieldVector<double,dim>& quadPos = quad[ip].position();

            const double weight = quad[ip].weight();

            const double integrationElement = element.geometry().integrationElement(quadPos);

            // Evaluate integral
            Dune::FieldMatrix<double,blocksize,dim> u_di;
            Dune::FieldMatrix<double,blocksize,dim> v_di;

            if (dynamic_cast<const VirtualGridViewFunction<GridView,Dune::FieldVector<double,blocksize> >*>(u))
                dynamic_cast<const VirtualGridViewFunction<GridView,Dune::FieldVector<double,blocksize> >*>(u)->evaluateDerivativeLocal(element, quadPos, u_di);
            else
                u->evaluateDerivative(element.geometry().global(quadPos), u_di);

            if (dynamic_cast<const VirtualGridViewFunction<GridView,Dune::FieldVector<double,blocksize> >*>(v))
                dynamic_cast<const VirtualGridViewFunction<GridView,Dune::FieldVector<double,blocksize> >*>(v)->evaluateDerivativeLocal(element, quadPos, v_di);
            else
                v->evaluateDerivative(element.geometry().global(quadPos), v_di);

            u_di -= v_di;  // compute the difference
            norm += u_di.frobenius_norm2() * weight * integrationElement;
        }
    }
    return norm;
    }


};

#endif

