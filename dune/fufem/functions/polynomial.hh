// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#ifndef POLYNOMIAL_HH
#define POLYNOMIAL_HH

#include <map>
#include <memory>

#include <dune/fufem/functions/virtualdifferentiablefunction.hh>

/** \brief  A class implementing polynomials \f$ P:\mathbb{R}\rightarrow\mathbb{R} \f$ of arbitrary degree.
 *
 *  The polynomial expects the coefficients of the monomial representation and implements its evaluation and
 *  its derivatives of arbitrary order.
 *  \tparam  DT Domain Field type
 *  \tparam  RT Range Field type
 */
template < class DT, class RT >
class Polynomial:
    public VirtualDifferentiableFunction<DT,RT>
{
    private:
        typedef VirtualDifferentiableFunction<DT,RT> BaseType;


    public:
        typedef std::vector<RT> CoeffType;

        typedef typename BaseType::DomainType DomainType;
        typedef typename BaseType::RangeType RangeType;
        typedef typename BaseType::DerivativeType DerivativeType;

        /** \brief Constructor
         *
         *  \param coeffs vector containing the coefficients of the monomial representation
         */
        Polynomial(const CoeffType coeffs):
            coeffs_(coeffs),
            degree_(coeffs.size()-1)
        {}

        /** \brief polynomial degree
         */
        int degree() const
        {
            return degree_;
        }

        /** \brief evaluation of the polynomial at point x
         *
         *  The polynomial is evaluated efficiently using Horner's scheme
         *  \param x the point at which to evaluate
         */
        RangeType evaluate(const DomainType& x) const
        {
            RangeType p = 0.0;

            for (int k=degree_; k>=0; --k)
                p = x*p + coeffs_[k];

            return p;
        }

        /** \brief evaluation of the polynomial at point x
         *
         *  The polynomial is evaluated efficiently using Horner's scheme
         *  \param x the point at which to evaluate
         *  \param y the object to store the function value in
         */
        virtual void evaluate(const DomainType& x, RangeType& y) const
        {
            y = evaluate(x);
            return;
        }


        /** \brief evaluation of derivative
         *
         *  \param x point at which to evaluate
         *  \param d object to store the derivative
         */
        virtual void evaluateDerivative(const DomainType& x, DerivativeType& d) const
        {
            d = 0.0;

            for (int k=degree_; k>=1; --k)
                d = x*d + dCoeff(1,k)*coeffs_[k];

            return;
        }

        /** \brief Returns the derivative function as a Polynomial object
         *
         *  \param d order of the derivative
         */
        Polynomial<DomainType, RangeType>& D(const std::size_t d)
        {
            if (d==0)
              return *this;

            typename DerivativeContainer::iterator ret=dP.find(d);
            if (ret==dP.end())
            {
                CoeffType newCoeffs = coeffs_;

                for (std::size_t k = degree_; k>=d; --k)
                    newCoeffs[k] *= dCoeff(d,k);

                for (std::size_t k=0; k<d; ++k)
                {
                    typename CoeffType::iterator firstEntry=newCoeffs.begin();
                    newCoeffs.erase(firstEntry);
                }

                ret = dP.insert(std::make_pair(d,std::make_shared<Polynomial<DT,RT> >(newCoeffs))).first;
            }

            return *(ret->second);
        }

        ~Polynomial()
        {}

    private:
        const CoeffType coeffs_;
        const unsigned int degree_;

        typedef std::map<std::size_t, std::shared_ptr<Polynomial<DT,RT> > > DerivativeContainer;
        DerivativeContainer dP;

        unsigned int dCoeff(const int& d, const int& k) const
        {
            unsigned int r=1;
            for (int i=0; i<d; ++i)
                r *= k-i;

            return r;
        }

};


#endif
