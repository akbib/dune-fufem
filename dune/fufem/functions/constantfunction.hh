// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef CONSTANT_FUNCTION_HH
#define CONSTANT_FUNCTION_HH

#include <type_traits>

#include <dune/fufem/functions/virtualdifferentiablefunction.hh>

/** \brief  A class wrapping a constant function
 *
 *  \tparam  DT DomainType
 *  \tparam  RT RangeType
 */
template < class DT, class RT >
class ConstantFunction:
    public VirtualDifferentiableFunction<DT,RT>
{
        typedef VirtualDifferentiableFunction<DT,RT> BaseType;


        // The following two methods provide the actual
        // implementation of evaluateDerivative().
        // In general there is only a special method for
        // the dummy type DerivativeTypeNotImplemented
        // that does not change the DerivativeType object
        // handed to it. This is necessary if there is
        // no proper type available for the implementation
        // of the derivative.
        //
        // Only if the DerivativeType is not equal to
        // DerivativeTypeNotImplemented (i.e. if there
        // is an implementation of the derivative)
        // we enable a method that sets the DerivativeType
        // object to zero.
        //
        // We could also change the base class in the
        // former case using enable_if with template
        // specialization in the following way:
        //
        // template < class DT, class RT , class SFINAE_DUMMY=void>
        // class ConstantFunction:
        //     public VirtualDifferentiableFunction<DT,RT>
        // {
        //     [...]
        // };
        //
        //
        // template < class DT, class RT >
        // class ConstantFunction <DT, RT,
        //       typename std::enable_if<
        //             std::is_same< typename DerivativeTypefier<DT, RT>::DerivativeType, DerivativeTypeNotImplemented>::value
        //             >::type >:
        //     public Dune::VirtualFunction<DT,RT>
        // {
        //     [...]
        // };

        static void setDerivativeZero(DerivativeTypeNotImplemented& d)
        {}


        template<class T, typename std::enable_if<not(std::is_same<T, DerivativeTypeNotImplemented>::value),int >::type=0>
        static void setDerivativeZero(T& d)
        {
            d=0;
        }

    public:

        typedef typename BaseType::DomainType DomainType;
        typedef typename BaseType::RangeType RangeType;
        typedef typename BaseType::DerivativeType DerivativeType;

        /** \brief Constructor
         *
         * \param constant Value of the constant function
         */
        ConstantFunction(const RangeType constant):
            constant_(constant)
        {}

        /** \brief evaluate function
         *
         *  \param x the point at which to evaluate
         *  \param y variable to store result in
         */
        virtual void evaluate(const DomainType& x, RangeType& y) const
        {
            y = constant_;
            return;
        }

        /** \brief evaluate derivative
         *
         *  \param x the point at which to evaluate
         *  \param d object to store result in
         */
        virtual void evaluateDerivative(const DomainType& x, DerivativeType& d) const
        {
            setDerivativeZero(d);
            return;
        }

    private:
        const RangeType constant_;
};


#endif

