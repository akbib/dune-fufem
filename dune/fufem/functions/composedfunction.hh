// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef COMPOSED_FUNCTION_HH
#define COMPOSED_FUNCTION_HH

#include <dune/common/function.hh>

/** \brief  A class implementing the composition of two functions.
 *
 *  The composed function \f$ f\circ g:A\rightarrow C \f$ where \f$ g:A\rightarrow B \f$ and \f$ f:B\rightarrow C \f$
 *  is implemented.
 *  \tparam  AT type of \f$ domain(g)\f$
 *  \tparam  BT type of \f$ range(g)\f$ and \f$ domain(f)\f$
 *  \tparam  CT type of \f$ range(f)\f$
 */
template <typename AT, typename BT, typename CT>
class ComposedFunction
    : public Dune::VirtualFunction<AT, CT>
{
    typedef Dune::VirtualFunction<AT, CT> BaseType;

public:
    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType RangeType;

    typedef Dune::VirtualFunction<AT,BT> InnerFunctionType;
    typedef Dune::VirtualFunction<BT,CT> OuterFunctionType;

    /** \brief Constructor
     *
     *  \param f_ outer function for composition
     *  \param g_ inner function for composition
     */
    ComposedFunction(const OuterFunctionType& f_, const InnerFunctionType& g_) :
        f(f_),
        g(g_)
    {}

    /** \brief composed evaluation of one component using eval of the composed functions
     *
     *  \param x point in \f$ A \f$ at which to evaluate
     *  \param y vector in \f$ C \f$ to store the function value in
     */
    virtual void evaluate(const DomainType& x,  RangeType& y) const
    {
        BT ytemp;

        g.evaluate(x,ytemp);
        f.evaluate(ytemp,y);
    }

    ~ComposedFunction(){}

private:
    const OuterFunctionType& f;
    const InnerFunctionType& g;

};

#endif

