#include <config.h>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/fufem/functiontools/basisinterpolator.hh>

#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include "common.hh"


template<class DT, class RT>
class F:
    public Dune::VirtualFunction<DT, RT>
{
    public:
        void evaluate(const DT& x, RT& y) const
        {
            y = 1;
        }
};


struct Suite
{
    template<class GridType>
    bool check(const GridType& grid)
    {
        bool passed = true;

        passed = passed and checkForRangeType<GridType, typename Dune::FieldVector<double, 1> >(grid);
        passed = passed and checkForRangeType<GridType, typename Dune::FieldVector<double, 2> >(grid);
        passed = passed and checkForRangeType<GridType, typename Dune::FieldVector<double, 3> >(grid);

        passed = passed and checkForRangeType<GridType, typename Dune::FieldVector<int, 1> >(grid);
        passed = passed and checkForRangeType<GridType, typename Dune::FieldVector<int, 2> >(grid);
        passed = passed and checkForRangeType<GridType, typename Dune::FieldVector<int, 3> >(grid);

        passed = passed and checkForRangeType<GridType, typename Dune::FieldMatrix<double, 2, 2> >(grid);
        passed = passed and checkForRangeType<GridType, typename Dune::FieldMatrix<double, 2, 3> >(grid);
        passed = passed and checkForRangeType<GridType, typename Dune::FieldMatrix<double, 3, 2> >(grid);

        passed = passed and checkForRangeType<GridType, typename Dune::FieldMatrix<int, 2, 2> >(grid);
        passed = passed and checkForRangeType<GridType, typename Dune::FieldMatrix<int, 2, 3> >(grid);
        passed = passed and checkForRangeType<GridType, typename Dune::FieldMatrix<int, 3, 2> >(grid);

        return passed;
    }

    template<class GridType, class RT>
    bool checkForRangeType(const GridType& grid)
    {
        typedef typename Dune::FieldVector<double, GridType::dimensionworld> DT;
        typename Dune::BlockVector<RT> v;

        F<DT, RT> f;
        DT x0(0);
        RT y0;
        f.evaluate(x0, y0);

        DuneFunctionsBasis<Dune::Functions::LagrangeBasis<typename GridType::LeafGridView, 1> > basis(grid.leafGridView());
        Functions::interpolate(basis, v, f);

        bool passed = (v.size() == basis.size());
        for (size_t i=0; i<v.size(); ++i)
            passed = passed and (v[i] == y0);
        return passed;
    }

};

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    Suite tests;

    bool passed = checkWithStandardAdaptiveGrids(tests);


    return passed ? 0 : 1;
}
