#include <config.h>
#include <dune/common/test/testsuite.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/indices.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/multitypeblockmatrix.hh>

#include <dune/fufem/assemblers/istlbackend.hh>

// Due to a formerly unnoticed bug, activate bounds checking:
#define DUNE_CHECK_BOUNDS 1

using namespace Dune;

template<class Entry, class Matrix, class RowIndex, class ColIndex>
TestSuite testISTLBackend(Matrix& mat, RowIndex&& row, ColIndex&& col) {
  TestSuite suite;

  auto backend = Fufem::istlMatrixBackend<Entry>(mat);

  suite.check(backend(row, col) == 1.0, "Check matrix access") << "Type" << typeid(mat).name() << " failed";

  return suite;
}

int main(int argc, char** argv) {
  MPIHelper::instance(argc, argv);

  TestSuite suite;

  using K = double;

  // Matrix<FieldMatrix<K, 1,1>
  {
    using Matrix = Matrix<FieldMatrix<K,1,1>>;
    Matrix mat(2,2);
    mat =1.0;

    // check with multiindex with length 1
    {
      auto idx = std::array<size_t, 1>{{1}};

      suite.subTest(testISTLBackend<K>(mat, idx, idx));

      // check const:
      const auto& const_mat = mat;
      suite.subTest(testISTLBackend<const K>(const_mat, idx, idx));
    }
    // check with multiindex with length 2
    {
      // someone might set a trailing zero to account for the matrix
      // character of FieldMatrix<K, 1,1>:
      auto idx = std::array<size_t, 2>{{1, 0}};

      suite.subTest(testISTLBackend<K>(mat, idx, idx));

      // check const:
      const auto& const_mat = mat;
      suite.subTest(testISTLBackend<const K>(const_mat, idx, idx));
    }
  }

  // Matrix<FieldMatrix<K, n,n> n>1
  {
    constexpr int n = 2;
    using Matrix = Matrix<FieldMatrix<K, n, n>>;
    Matrix mat(2,2);
    mat = 1.0;

    auto idx = std::array<size_t, 2>{{1, 1}};

    suite.subTest(testISTLBackend<K>(mat, idx, idx));

    // check const:
    const auto& const_mat = mat;
    suite.subTest(testISTLBackend<const K>(const_mat, idx, idx));
  }

  return suite.exit();
}
