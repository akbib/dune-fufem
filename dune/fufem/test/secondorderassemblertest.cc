// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#include <config.h>

#include <array>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>

#include <dune/grid/yaspgrid.hh>

#include <dune/istl/io.hh>
#include <dune/istl/scaledidmatrix.hh>

#include <dune/matrix-vector/axpy.hh>

#include <dune/fufem/assemblers/operatorassembler.hh>

#include <dune/fufem/assemblers/localassemblers/secondorderoperatorassembler.hh>

#include <dune/fufem/assemblers/localassemblers/laplaceassembler.hh>
#include <dune/fufem/assemblers/localassemblers/vvlaplaceassembler.hh>
#include <dune/fufem/assemblers/localassemblers/stvenantkirchhoffassembler.hh>
#include <dune/fufem/assemblers/localassemblers/viscosityassembler.hh>
#include <dune/fufem/assemblers/localassemblers/variablecoefficientviscosityassembler.hh>

#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>

template<class Matrix1, class Matrix2>
bool compare(const Matrix1& matrix1, const Matrix2& matrix2)
{
  if (matrix1.N()!=matrix2.N())
    DUNE_THROW(Dune::Exception, "Number of rows does not match");
  if (matrix1.M()!=matrix2.M())
    DUNE_THROW(Dune::Exception, "Number of rows does not match");

  for(std::size_t i=0; i<matrix1.N(); ++i)
  {
    auto it = matrix1[i].begin();
    auto end = matrix1[i].end();
    for(; it!=end; ++it)
    {
      if (matrix2[i].find(it.index()) == matrix2[i].end())
        DUNE_THROW(Dune::Exception, "Entry missing");
      auto z = *it;
      z -= matrix2[i][it.index()];
      if (z.infinity_norm() > 1e-10)
      {
        std::cout << *it << " != " << matrix2[i][it.index()] << std::endl;
        DUNE_THROW(Dune::Exception, "Entries are not equal");
      }
    }
  }

  return true;
}

template <class Lambda, class Domain, class RangeTEMPLATE>
class WrapLambda : public Dune::VirtualFunction<Domain,RangeTEMPLATE> {
  using Base = Dune::VirtualFunction<Domain,RangeTEMPLATE>;
public:
  using typename Base::RangeType;

  WrapLambda(Lambda lambda)
    : lambda_(lambda)
  {}

  void evaluate(Domain const &x, RangeType &y) const {
    y = lambda_(x);
  }

private:
  Lambda const lambda_;
};

int main (int argc, char *argv[])
{
  Dune::MPIHelper::instance(argc, argv);

//  const int dim = 2;
//  const int refine = 9;

  const int dim = 3;

#ifndef NDEBUG
  const int refine = 1;
#else
  const int refine = 4;
#endif

  // Build a test grid
  typedef Dune::YaspGrid<dim> GridType;

  Dune::FieldVector<double,dim> h(1);
  std::array<int,dim> n;
  n.fill(2);
  n[0] = 3;

  GridType grid(h,n);
  grid.globalRefine(refine);

  // Construct a function space basis for the grid
  typedef DuneFunctionsBasis<Dune::Functions::LagrangeBasis<GridType::LeafGridView, 1> > P1Basis;
  P1Basis p1Basis(grid.leafGridView());

  OperatorAssembler<P1Basis,P1Basis> globalAssembler(p1Basis, p1Basis);

  typedef Dune::FieldMatrix<double,dim,dim> LocalMatrix;
  typedef Dune::ScaledIdentityMatrix<double,dim> LocalIdentityMatrix;
  typedef Dune::BCRSMatrix<LocalMatrix> MatrixType;
  typedef GridType::Codim<0>::Geometry::GlobalCoordinate GlobalCoordinate;
  MatrixType matrix1;
  MatrixType matrix2;

  Dune::Timer timer;
  {

    LaplaceAssembler<GridType,P1Basis::LocalFiniteElement,P1Basis::LocalFiniteElement, LocalMatrix> laplaceAssembler;

    timer.reset();
    globalAssembler.assemble(laplaceAssembler, matrix1);
    std::cout << "Assembling with LaplaceAssembler took " << timer.elapsed() << "s" << std::endl;


    auto contraction = [](const GlobalCoordinate& g1, const GlobalCoordinate& g2) {
      return LocalIdentityMatrix(g1*g2);
    };
    SecondOrderOperatorAssembler<GridType, P1Basis::LocalFiniteElement, P1Basis::LocalFiniteElement, decltype(contraction), LocalMatrix> secondOrderAssembler(contraction, true);

    timer.reset();
    globalAssembler.assemble(secondOrderAssembler, matrix2);
    std::cout << "Assembling with SecondOrderOperatorAssembler took " << timer.elapsed() << "s" << std::endl;

    compare(matrix1, matrix2);
  }

  {
    LocalMatrix L(1);
    Dune::MatrixVector::addProduct(L, 1, LocalIdentityMatrix(1));

    auto const leafView = grid.leafGridView();
    auto funcL = [L](GlobalCoordinate const &) {return L;};
    auto gridFuncL = Dune::Functions::makeGridViewFunction(funcL, leafView);

    VVLaplaceAssembler<GridType,P1Basis::LocalFiniteElement,P1Basis::LocalFiniteElement, LocalMatrix> vvlaplaceAssembler(L);

    timer.reset();
    globalAssembler.assemble(vvlaplaceAssembler, matrix1);
    std::cout << "Assembling with VVLaplaceAssembler took " << timer.elapsed() << "s" << std::endl;

    auto contraction = [](const GlobalCoordinate& g1, const GlobalCoordinate& g2,
                          const LocalMatrix &L) {
      auto M = L;
      M *= (g1*g2);
      return M;
    };
    SecondOrderOperatorAssembler<GridType, P1Basis::LocalFiniteElement, P1Basis::LocalFiniteElement, decltype(contraction), LocalMatrix, decltype(gridFuncL)> secondOrderAssembler(contraction, true, QuadratureRuleKey(dim, 0), gridFuncL);

    timer.reset();
    globalAssembler.assemble(secondOrderAssembler, matrix2);
    std::cout << "Assembling with SecondOrderOperatorAssembler took " << timer.elapsed() << "s" << std::endl;

    compare(matrix1, matrix2);
  }

  {
    double const E = 2.8;
    double const nu = 0.23;

    StVenantKirchhoffAssembler<GridType,P1Basis::LocalFiniteElement,P1Basis::LocalFiniteElement> stVenantKirchhoffAssembler(E, nu);

    timer.reset();
    globalAssembler.assemble(stVenantKirchhoffAssembler, matrix1);
    std::cout << "Assembling with StVenantKirchhoffAssembler took " << timer.elapsed() << "s" << std::endl;

    auto const &stVenantKirchhoffAssemblerReplacement
      = getStVenantKirchhoffAssembler<GridType,
                                      P1Basis::LocalFiniteElement,
                                      LocalMatrix>(grid, E, nu);

    timer.reset();
    globalAssembler.assemble(stVenantKirchhoffAssemblerReplacement, matrix2);
    std::cout << "Assembling with SecondOrderOperatorAssembler took " << timer.elapsed() << "s" << std::endl;

    compare(matrix1, matrix2);
  }

  {
    double const muShear = 2.0;
    double const muBulk = 1.6;

    ViscosityAssembler<GridType,P1Basis::LocalFiniteElement,P1Basis::LocalFiniteElement> viscosityAssembler(muShear, muBulk);

    timer.reset();
    globalAssembler.assemble(viscosityAssembler, matrix1);
    std::cout << "Assembling with ViscosityAssembler took " << timer.elapsed() << "s" << std::endl;

    auto const &viscosityAssemblerReplacement =
        getIsotropicNewtonianViscosityAssembler<
            GridType, P1Basis::LocalFiniteElement, LocalMatrix>(grid, muShear,
                                                                muBulk);

    timer.reset();
    globalAssembler.assemble(viscosityAssemblerReplacement, matrix2);
    std::cout << "Assembling with SecondOrderOperatorAssembler took " << timer.elapsed() << "s" << std::endl;

    compare(matrix1, matrix2);
  }

  {
    auto const muShear
      = [](GlobalCoordinate const &x) {
      return x[1] <= 1.0 / 2.0 ? 1.0 : 2.0;
    };
    auto const muBulk
      = [](GlobalCoordinate const &x) {
      return (x[0] <= 1.0 / 3.0) ? 1.0 : ((x[0] <= 2.0 / 3.0) ? 2.0 : 3.0);
    };
    // The old classes require something with an evaluate() function, etc.
    WrapLambda<decltype(muShear), GlobalCoordinate, Dune::FieldVector<double, 1>>
      muShearWrapped(muShear);
    WrapLambda<decltype(muBulk), GlobalCoordinate, Dune::FieldVector<double, 1>>
      muBulkWrapped(muBulk);

    VariableCoefficientViscosityAssembler
      <GridType,P1Basis::LocalFiniteElement,P1Basis::LocalFiniteElement,
       Dune::VirtualFunction<GlobalCoordinate,Dune::FieldVector<double, 1>>>
      viscosityAssembler(grid, muShearWrapped, muBulkWrapped,
                         QuadratureRuleKey(dim, 0), QuadratureRuleKey(dim, 0));

    timer.reset();
    globalAssembler.assemble(viscosityAssembler, matrix1);
    std::cout << "Assembling with VariableCoefficientViscosityAssembler took " << timer.elapsed() << "s" << std::endl;

    auto const &viscosityAssemblerReplacement =
        getIsotropicNewtonianViscosityAssembler<
            GridType, P1Basis::LocalFiniteElement, LocalMatrix>(
            grid, muShear, muBulk, QuadratureRuleKey(dim, 0));

    timer.reset();
    globalAssembler.assemble(viscosityAssemblerReplacement, matrix2);
    std::cout << "Assembling with SecondOrderOperatorAssembler took " << timer.elapsed() << "s" << std::endl;

    compare(matrix1, matrix2);
  }
}
