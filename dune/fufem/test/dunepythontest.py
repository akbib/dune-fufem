from numpy import *
from numpy.linalg import norm

foo='foo'
bar='bar'

def add(x,y):
    return x+y

int_list=[11,12,13]
str_list=["string1","string2","string3"]
int_tuple=(1,2,3)



# some utilities for creating parametrized boundaries

class BoundarySegment(list):
    """ A python BoundarySegment representation

    It behaves like a list and function at the same time.
    You can access the passed vertices using [] and
    call the passed parametrization usin ().
    """
    def __init__(self, vertices, parametrization):
        super(BoundarySegment,self).__init__(vertices)
        self.parametrization = parametrization

    def __call__(self, x):
        return self.parametrization(x)

class TransformedBoundarySegment(BoundarySegment):
    """ This transforms a local parametrization into global one

    The local parametrization is assumed to be a curve f
    on [0,1] with f(0)=(0,0) and f(1)=(1,0).
    This class rotatetes, scales, and translates the graph
    of the curve such that the transformed curve Tf satisfies
    Tf(0) = coordinates(vertices[0]) and
    Tf(1) = coordinates(vertices[1]).
    The scaling in normal direction is done with the
    same factor such that T is angle and curvature preserving.
    """
    def __init__(self, vertices, localParametrization, allCoordinates):
        coordinates = array([allCoordinates[i] for i in vertices])
        edge = coordinates[1]-coordinates[0]
        normal = (-edge[1], edge[0])
        parametrization = lambda x: dot(transpose(array((edge, normal))), localParametrization(x)) + coordinates[0]
        super(TransformedBoundarySegment, self).__init__(vertices, parametrization)

class ArcOverSecant:
    """ Parametrization of a circle arc over a secant

    The circle arc is parametrized over [0,1] and
    placed such that its secant line goes from
    (0,0) to (1,0). The radius of the arc is
    selected automatically such that the
    arc has the given angle.
    """
    def __init__(self, angle):
        self.alphaHalf = float(angle)/2.0
        self.invSinAlphaHalf = 1.0/sin(self.alphaHalf)

    def __call__(self, x):
        betaHalf = self.alphaHalf*x[0]
        secantlength = sin(betaHalf)*self.invSinAlphaHalf
        secantangle = self.alphaHalf-betaHalf
        return (cos(secantangle)*secantlength, sin(secantangle)*secantlength)





# reentrent corner circle grid

class Grid:
    def __init__(self):
        self.vertices = [(0,0), (1,0), (0,1), (-1,0), (0, -1)]
        self.elements = [(0,1,2), (0,2,3), (0,3,4)]

        # We use some utility classes here, but each segment essentially needs to be iterable,
        # e.g, a list. If it is additionally callable, it is used as parametrization.
        self.segments = []
        self.segments.append(TransformedBoundarySegment((2,1), ArcOverSecant(2.0*pi/4), self.vertices))
        self.segments.append(TransformedBoundarySegment((3,2), ArcOverSecant(2.0*pi/4), self.vertices))
        self.segments.append(TransformedBoundarySegment((4,3), ArcOverSecant(2.0*pi/4), self.vertices))

grid = Grid()

