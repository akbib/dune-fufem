#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <limits>

#include <dune/common/exceptions.hh>

#include <dune/fufem/test/common.hh>
#include <dune/fufem/geometry/refinedsimplexgeometry.hh>

class RefinedSimplexGeometryTestSuite
{
    public:
    typedef typename RefinedSimplexGeometry<double,2,3>::GlobalCoordinate WorldCoords;
    typedef typename RefinedSimplexGeometry<double,2,3>::LocalCoordinate LocalCoords;
    typedef typename RefinedSimplexGeometry<double,2,3>::JacobianTransposed JacobianTransposed;
    typedef typename RefinedSimplexGeometry<double,2,3>::JacobianInverseTransposed::Base JacobianInverseTransposedBase;

    RefinedSimplexGeometryTestSuite():
      vertices{{WorldCoords{1.0, 1.0, 1.0}, WorldCoords{5.0, 0.0, 2.0}, WorldCoords{2.5, 3.0, -2.0}, WorldCoords{3.0, 2.0, 0.0}, WorldCoords{2.0, 2.0, 0.0}, WorldCoords{3.0, 2.0, -1.0}}},
        testGeometry(vertices)
    {
        jacobianTransposed[0] = {{4, 2, -2}, {2, 2, -2}};
        jacobianTransposed[1] = {{4, -4, 4}, {0, 0, -2}};
        jacobianTransposed[2] = {{2, 0, -2}, {1, 2, -4}};
        jacobianTransposed[3] = {{2, 0, -2}, {0, 0, -2}};

        jacobianInverseTransposed[0] = {{0.5, -0.5},{-0.25, 0.5},{0.25, -0.5}};
        jacobianInverseTransposed[1] = {{0.125, 0.25},{-0.125, -0.25},{-0.0, -0.5}};
        jacobianInverseTransposed[2] = {{16.0/34.0, -6.0/34.0},{-10.0/34.0, 8.0/34.0},{-1.0/34.0, -6.0/34.0}};
        jacobianInverseTransposed[3] = {{0.5, -0.5},{0.0, 0.0},{0.0, -0.5}};

        integrationElement[0] = std::sqrt(32.0);
        integrationElement[1] = std::sqrt(128.0);
        integrationElement[2] = std::sqrt(68.0);
        integrationElement[3] = 4.0;

        testCoords = {{LocalCoords{0.2, 0.05},LocalCoords{0.55, 0.4},LocalCoords{0.45, 0.51},LocalCoords{0.4,0.4}}};
    }


    bool check()
    {
        return true and
               check_affine() and
               check_type() and
               check_corners() and
               check_corner() and
               check_center() and
               check_global() and
               check_local() and
               check_integrationElement() and
               check_volume() and
               check_jacobianTransposed() and
               check_jacobianInverseTransposed();
    }

    private:

    bool check_affine()
    {
        return not testGeometry.affine();
    }

    bool check_type()
    {
        bool type_throws_NotImplemented = false;
        try
        {
        testGeometry.type();
        }

        catch (Dune::NotImplemented e)
        {
            type_throws_NotImplemented= true;
        }

        return type_throws_NotImplemented;
    }

    bool check_corners()
    {
        return testGeometry.corners() == 6;
    }

    bool check_corner()
    {
        bool passed = true;
        for (int i=0; i<6; ++i)
            passed = passed and isCloseDune(testGeometry.corner(i),vertices[i]);

        return passed;
    }

    bool check_center()
    {
        bool center_throws_NotImplemented = false;
        try
        {
            testGeometry.center();
        }
        catch (Dune::NotImplemented e)
        {
            center_throws_NotImplemented= true;
        }
        return center_throws_NotImplemented;
    }

    bool check_global()
    {
        bool passed = true;
        passed = passed and isCloseDune(testGeometry.global(LocalCoords{0.0, 0.0}),vertices[0]);
        passed = passed and isCloseDune(testGeometry.global(LocalCoords{1.0, 0.0}),vertices[1]);
        passed = passed and isCloseDune(testGeometry.global(LocalCoords{0.0, 1.0}),vertices[2]);
        passed = passed and isCloseDune(testGeometry.global(LocalCoords{0.5, 0.0}),vertices[3]);
        passed = passed and isCloseDune(testGeometry.global(LocalCoords{0.5, 0.5}),vertices[5]);
        passed = passed and isCloseDune(testGeometry.global(LocalCoords{0.0, 0.5}),vertices[4]);

        passed = passed and isCloseDune(testGeometry.global(LocalCoords{1.0/6.0, 1.0/6.0}),WorldCoords{2.0, 5.0/3.0, 1.0/3.0});
        passed = passed and isCloseDune(testGeometry.global(LocalCoords{4.0/6.0, 1.0/6.0}),WorldCoords{11.0/3.0, 4.0/3.0, 1.0/3.0});
        passed = passed and isCloseDune(testGeometry.global(LocalCoords{2.0/6.0, 2.0/6.0}),WorldCoords{8.0/3.0, 2.0, -1.0/3.0});
        passed = passed and isCloseDune(testGeometry.global(LocalCoords{1.0/6.0, 4.0/6.0}),WorldCoords{2.5, 7.0/3.0, -1.0});

        return passed;
    }

    bool check_local()
    {
        bool local_throws_NotImplemented = false;
        try
        {
            testGeometry.local(vertices[0]);
        }

        catch (Dune::NotImplemented e)
        {
            local_throws_NotImplemented= true;
        }
        return local_throws_NotImplemented;
    }

    bool check_volume()
    {
        bool volume_throws_NotImplemented = false;
        try
        {
            testGeometry.volume();
        }

        catch (Dune::NotImplemented e)
        {
            volume_throws_NotImplemented= true;
        }
        return volume_throws_NotImplemented;
    }

    bool check_integrationElement()
    {
        bool passed = true;
        for (std::size_t i=0; i<4; ++i)
            passed = passed and isCloseAbs(testGeometry.integrationElement(testCoords[i]),integrationElement[i]);

        return passed;
    }

    bool check_jacobianTransposed()
    {
        bool passed = true;
        for (std::size_t i=0; i<4; ++i)
            passed = passed and isCloseDune(testGeometry.jacobianTransposed(testCoords[i]),jacobianTransposed[i]);

        return passed;
    }

    bool check_jacobianInverseTransposed()
    {
        bool passed = true;
        for (std::size_t i=0; i<4; ++i)
        {
            auto jit = testGeometry.jacobianInverseTransposed(testCoords[i]);
            passed = passed and isCloseDune(static_cast<JacobianInverseTransposedBase&>(jit),jacobianInverseTransposed[i]);
        }
        return passed;
    }


    std::array<WorldCoords, 6> vertices;
    std::array<JacobianTransposed, 4>  jacobianTransposed;
    std::array<JacobianInverseTransposedBase, 4>  jacobianInverseTransposed;
    std::array<double, 4> integrationElement;
    RefinedSimplexGeometry<double,2,3> testGeometry;

    std::array<LocalCoords,4> testCoords;

};

int main (int argc, char *argv[])
{
      RefinedSimplexGeometryTestSuite testSuite;
      return not testSuite.check();
}
