#include <config.h>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/function.hh>

#include <dune/fufem/assemblers/integraloperatorassembler.hh>
#include <dune/fufem/assemblers/localassemblers/convolutionassembler.hh>

#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include "common.hh"



template <int N>
class Mollifier :
    public Dune::VirtualFunction<std::pair<Dune::FieldVector<double, N>, Dune::FieldVector<double, N> >, double>
{
    private:
        typedef typename Dune::template FieldVector<double, N> SingleDomain;
        typedef typename std::template pair<SingleDomain, SingleDomain> PairDomain;
        typedef typename Dune::template VirtualFunction<PairDomain, double> Base;

    public:
        typedef typename Base::RangeType RangeType;
        typedef typename Base::DomainType DomainType;

        Mollifier(double eps) :
            eps_(eps)
        {}

        void evaluate(const DomainType& xy, RangeType& z) const
        {
            SingleDomain diff = xy.first;
            diff -= xy.second;
            diff /= eps_;
            z = exp(-1.0/(1.0-diff.two_norm2()));
        }

    private:
        double eps_;
};


struct IntegralOperatorAssemblerTestSuite
{
    template<class GridType>
    bool check(const GridType& grid)
    {
        const int dim = GridType::dimensionworld;


        Mollifier<dim> m(0.1);

        typedef DuneFunctionsBasis<Dune::Functions::LagrangeBasis<typename GridType::LeafGridView, 1> > Basis;
        Basis basis(grid.leafGridView());

        typename Dune::Matrix<Dune::FieldMatrix<double,1,1> > M1, M2;

        IntegralOperatorAssembler<Basis, Basis> assembler(basis, basis);
        ConvolutionAssembler<Basis, Basis> convolutionAssembler(m);

        assembler.assemble(convolutionAssembler, M1);
        assembler.assemble(convolutionAssembler, M2, true);

        M1-=M2;

        return (M1.infinity_norm() < 1e-14);
    }

};


int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    IntegralOperatorAssemblerTestSuite tests;

    bool passed = true;

#if HAVE_UG
    passed = passed and checkWithAdaptiveGrid<Dune::UGGrid<2> >(tests, 1, 3);
    passed = passed and checkWithAdaptiveGrid<Dune::UGGrid<3> >(tests, 1, 1);
#endif

#if HAVE_DUNE_ALUGRID
    passed = passed and checkWithAdaptiveGrid<Dune::ALUGrid<2,2, Dune::simplex, Dune::nonconforming> >(tests, 1, 3);
    passed = passed and checkWithAdaptiveGrid<Dune::ALUGrid<3,3, Dune::simplex, Dune::nonconforming> >(tests, 1, 1);
#endif

    return passed ? 0 : 1;
}
