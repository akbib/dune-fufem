#include <config.h>

#include <cstdio>
#include <map>
#include <vector>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/bitsetvector.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/io/file/amirameshwriter.hh>

#if HAVE_DUNE_SUBGRID
#include <dune/subgrid/subgrid.hh>
#endif

#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include <dune/fufem/functionspacebases/conformingbasis.hh>

#include <dune/fufem/functions/basisgridfunction.hh>
//#include <dune/fufem/functions/virtualgridfunction.hh>
//#include <dune/fufem/functions/virtualdifferentiablefunction.hh>

#include <dune/fufem/functiontools/basisinterpolator.hh>
#include <dune/fufem/functiontools/amirameshbasiswriter.hh>

#include <dune/fufem/assemblers/functionalassembler.hh>
#include <dune/fufem/assemblers/localassemblers/subgridh1functionalassembler.hh>
#include <dune/fufem/assemblers/localassemblers/subgridl2functionalassembler.hh>

#include "common.hh"

/** \brief sets up a transfer operator from subgridleaf to hostgridleaf where minlevel of subgrid elements is hostgrid.maxlevel-1
 *
 */
template<class TransferOperatorType, class SubGridBasis, class HostGridBasis>
void setupSubgridToHostgridTransfer(TransferOperatorType& matrix, const SubGridBasis& subgridbasis, const HostGridBasis& hostgridbasis)
{
    typedef typename SubGridBasis::GridView::Grid SubGridType;
    typedef std::map<int, double> LinearCombination;
    typedef std::vector<LinearCombination> BasisTransformation;
    typedef typename SubGridBasis::LocalFiniteElement CLFE;
    typedef typename HostGridBasis::LocalFiniteElement FLFE;
    typedef typename CLFE::Traits::LocalBasisType::Traits::RangeType CFERange;

    static const int dim = SubGridType::dimension;

    const SubGridType& subgrid=subgridbasis.getGridView().grid();
    // HostGridType& hostgrid=subgrid.getHostGrid();

    BasisTransformation transformation(hostgridbasis.size());

    // set all nodes as not processed
    Dune::BitSetVector<1> processed(hostgridbasis.size(),false);

    // loop over all elements of current level
    for(const auto& c : elements(subgrid.leafGridView()))
    {
        const auto& hostElement = subgrid.template getHostEntity<0>(c);
        const CLFE& coarseFE = subgridbasis.getLocalFiniteElement(c);

        // if element is leaf the transfer to the next level is locally the identity
        if (hostElement.isLeaf())
        {
            for (size_t j=0; j<coarseFE.localBasis().size(); ++j)
            {
                int coarseIndex = subgridbasis.index(c, j);
                int fineIndex = hostgridbasis.index(hostElement, j);

                // visit each host node only once
                if (processed[fineIndex][0])
                    continue;

                transformation[fineIndex][coarseIndex] = 1.0;
                processed[fineIndex][0] = true;
            }
        }
        else
        {
            int level = c.level();

            // store coarse node indices since we need them often
            std::vector<int> coarseIndex(coarseFE.localBasis().size());
            for (size_t i=0; i<coarseFE.localBasis().size(); ++i)
                coarseIndex[i] = subgridbasis.index(c, i);

            std::vector<CFERange> valuesAtPosition(coarseFE.localBasis().size());

            // loop over all children on next level
            auto fIt = hostElement.hbegin(level+1);
            auto fEnd = hostElement.hend(level+1);
            for (; fIt != fEnd; ++fIt)
            {
                const FLFE& fineFE = hostgridbasis.getLocalFiniteElement(*fIt);

                // we need the reference element to get the local position of the subentities corresponding to fine basis functions
                auto fineRefElement = Dune::ReferenceElements<double, dim>::general(fIt->type());

                // loop over all child nodes
                for (size_t j=0; j<fineFE.localBasis().size(); ++j)
                {
                    int fineIndex = hostgridbasis.index(*fIt, j);

                    // visit each node only once
                    if (processed[fineIndex][0])
                        continue;

                    // get local coordinates of subentity in fine element
                    const Dune::LocalKey& localKey = fineFE.localCoefficients().localKey(j);
                    Dune::FieldVector<double,dim> localPositionFine = fineRefElement.position(localKey.subEntity(), localKey.codim());

                    // compute local coordinates of subentity in coarse element
                    Dune::FieldVector<double, dim> localPositionCoarse = fIt->geometryInFather().global(localPositionFine);

                    // evaluate coarse basis functions at the position of the subentity corresponding to the fine basis function
                    coarseFE.localBasis().evaluateFunction(localPositionCoarse, valuesAtPosition);

                    for (size_t i=0; i<coarseFE.localBasis().size(); ++i)
                    {
                        if (valuesAtPosition[i] > 1e-5)
                            transformation[fineIndex][coarseIndex[i]] = valuesAtPosition[i];
                    }

                    processed[fineIndex][0] = true;
                } // loop over all child nodes
            } // end of loop over all children on next level
        }
    } // end of loop over all elements of current level

    // setup transfer operator matrices
    Dune::MatrixIndexSet indices(hostgridbasis.size(), subgridbasis.size());

    for (size_t row=0; row<transformation.size(); ++row)
    {
        LinearCombination::iterator colIt = transformation[row].begin();
        LinearCombination::iterator colEnd = transformation[row].end();
        for(; colIt!=colEnd; ++colIt)
            indices.add(row,colIt->first);
    }

    indices.exportIdx(matrix);

    for (size_t row=0; row<transformation.size(); ++row)
    {
        LinearCombination::iterator colIt = transformation[row].begin();
        LinearCombination::iterator colEnd = transformation[row].end();
        for(; colIt!=colEnd; ++colIt)
        {
            matrix[row][colIt->first] = 0.0;
            for(int i=0; i<TransferOperatorType::block_type::rows; ++i)
                matrix[row][colIt->first][i][i] = colIt->second;
        }
    }

}

/** \brief TestSuite for SubgridL2FunctionalAssembler
 *
 *  This TestSuite tests for consistency of the SubgridL2FunctionalAssembler with assembling on the hostgrid via L2FunctionalAssembler and restricting to the subgrid
 */
struct SubgridL2FunctionalAssemblerTestSuite
{
    template <typename HostGridType>
    bool check(HostGridType& hostgrid)
    {
#if HAVE_DUNE_SUBGRID
        typedef Dune::SubGrid<HostGridType::dimension, HostGridType> GridType;
        GridType grid(hostgrid);

        grid.createBegin();

        /* For test purposes we don't want the subgrid to resolve the whole hostgrid... */
        grid.insertLevel(hostgrid.maxLevel()-1);

        /* ...but we do want it to contain some hostgrid leafelements */
        auto helt=hostgrid.leafGridView().template begin<0>();
        auto hend=hostgrid.leafGridView().template end<0>();

        for (int n_elts=0; helt!=hend and n_elts<hostgrid.size(0)/4; ++helt, ++n_elts)
            grid.insert(*helt);

        grid.createEnd();

        /* make sure these requirements are met */
        assert(grid.size(0)!=hostgrid.size(0));

        static const int block_size=2;

        typedef DuneFunctionsBasis<Dune::Functions::LagrangeBasis<typename GridType::LeafGridView, 1> > NCBasis;
        typedef DuneFunctionsBasis<Dune::Functions::LagrangeBasis<typename HostGridType::LeafGridView, 1> > NCHostBasis;
        typedef ConformingBasis<NCBasis> Basis;
        typedef ConformingBasis<NCHostBasis> HostBasis;
        typedef Dune::BCRSMatrix<Dune::FieldMatrix<double,block_size,block_size> > Transfer;

        NCBasis ncbasis(grid.leafGridView());
        NCHostBasis nchostbasis(hostgrid.leafGridView());

        Basis basis(ncbasis);
        HostBasis hostbasis(nchostbasis);

        Transfer transfer;

        setupSubgridToHostgridTransfer<Transfer,Basis,HostBasis>(transfer,basis,hostbasis);

        /* the function to be plugged into the assembler */
        typedef Dune::BlockVector<Dune::FieldVector<double,block_size> > CoeffType;
        CoeffType coeffs(hostbasis.size());
        coeffs = 0.0;
        typedef BasisGridFunction<HostBasis,CoeffType> Function;
        Function function(hostbasis, coeffs);
        for (size_t i=0; i<coeffs.size(); ++i)
            for (int j=0; j<block_size; ++j)
                coeffs[i][j] = (2.0*rand())/RAND_MAX - 1.0;

        /* containers for assembled functionals */
        Dune::BlockVector<Dune::FieldVector<double,block_size> > g(basis.size()),
                                                                 gsubcoeffs(basis.size()),
                                                                 ghost(hostbasis.size());

        /* create assemblers */
        FunctionalAssembler<Basis> assembler(basis);
        FunctionalAssembler<HostBasis> hostassembler(hostbasis);

        SubgridL2FunctionalAssembler<GridType, typename Basis::LocalFiniteElement, Dune::FieldVector<double,block_size> > subgridl2functionalassembler(function,grid);
        L2FunctionalAssembler<HostGridType, typename HostBasis::LocalFiniteElement, Dune::FieldVector<double,block_size> > l2functionalassembler(function);

        /* assemble l2 functionals */
        assembler.assemble(subgridl2functionalassembler, g);
        hostassembler.assemble(l2functionalassembler, ghost);

        transfer.mtv(ghost,gsubcoeffs);

        assert(gsubcoeffs.size()==g.size());

        for (size_t j=0; j<basis.size(); ++j)
        {
            for (int k=0; k<block_size; ++k)
            {
                if (g[j][k]-gsubcoeffs[j][k]>1e-14)
                {
                    std::cout << "Problem detected: g[" << j << "," << k << "]= " << g[j][k] << "  gsubcoeffs[" << j << "," << k << "]= " << gsubcoeffs[j][k] << std::endl;;
                    return false;
                }
            }
        }
#endif
        return true;
    }
};

/** \brief TestSuite for SubgridH1FunctionalAssembler
 *
 *  This TestSuite tests for consistency of the SubgridH1FunctionalAssembler with assembling on the hostgrid via H1FunctionalAssembler and restricting to the subgrid
 */
struct SubgridH1FunctionalAssemblerTestSuite
{
    template <typename HostGridType>
    bool check(HostGridType& hostgrid)
    {
#if HAVE_DUNE_SUBGRID
        typedef Dune::SubGrid<HostGridType::dimension, HostGridType> GridType;
        GridType grid(hostgrid);

        grid.createBegin();

        /* For test purposes we don't want the subgrid to resolve the whole hostgrid... */
        grid.insertLevel(hostgrid.maxLevel()-1);

        /* ...but we do want it to contain some hostgrid leafelements */
        auto helt=hostgrid.leafGridView().template begin<0>();
        auto hend=hostgrid.leafGridView().template end<0>();

        for (int n_elts=0; helt!=hend and n_elts<hostgrid.size(0)/4; ++helt, ++n_elts)
            grid.insert(*helt);

        grid.createEnd();

        /* make sure these requirements are met */
        assert(grid.size(0)!=hostgrid.size(0));

        static const int block_size=1;

        typedef DuneFunctionsBasis<Dune::Functions::LagrangeBasis<typename GridType::LeafGridView, 1> > NCBasis;
        typedef DuneFunctionsBasis<Dune::Functions::LagrangeBasis<typename HostGridType::LeafGridView, 1> > NCHostBasis;
        typedef ConformingBasis<NCBasis> Basis;
        typedef ConformingBasis<NCHostBasis> HostBasis;
        typedef Dune::BCRSMatrix<Dune::FieldMatrix<double,block_size,block_size> > Transfer;

        NCBasis ncbasis(grid.leafGridView());
        NCHostBasis nchostbasis(hostgrid.leafGridView());

        Basis basis(ncbasis);
        HostBasis hostbasis(nchostbasis);

        Transfer transfer;

        setupSubgridToHostgridTransfer<Transfer,Basis,HostBasis>(transfer,basis,hostbasis);

        /* print transfer matrix to screen */
//        for (size_t i=0; i<transfer.N(); ++i)
//        {
//            for (size_t j=0; j<transfer.M(); ++j)
//            {
//                if (transfer.exists(i,j))
//                    std::cout << std::setw(3) << transfer[i][j] << " ";
//                else
//                    std::cout << "0.0 ";
//            }
//            std::cout << std::endl;
//        }

        /* the function to be plugged into the assembler */
        typedef std::vector<typename DerivativeTypefier<typename GridType::template Codim<0>::Geometry::GlobalCoordinate, Dune::FieldVector<double,block_size> >::DerivativeType > CoeffType;
        typedef BasisGridFunction<HostBasis,CoeffType> Function;
        typedef typename Function::RangeType RangeType;
        CoeffType coeffs(hostbasis.size());
        coeffs.assign(hostbasis.size(), RangeType(0.0));
        for (size_t i=0; i<coeffs.size(); ++i)
            for (int j=0; j<RangeType::rows; ++j)
                for (int k=0; k<RangeType::cols; ++k)
                    coeffs[i][j][k] = (2.0*rand())/RAND_MAX - 1.0;

        Function function(hostbasis, coeffs);

        /* containers for assembled functionals */
        Dune::BlockVector<Dune::FieldVector<double,block_size> > g(basis.size()),
                                                                 gsubcoeffs(basis.size()),
                                                                 ghost(hostbasis.size());

        g = gsubcoeffs = 0.0;
        ghost = 0.0;

        /* create assemblers */
        FunctionalAssembler<Basis> assembler(basis);
        FunctionalAssembler<HostBasis> hostassembler(hostbasis);

        SubgridH1FunctionalAssembler<GridType, typename Basis::LocalFiniteElement, Dune::FieldVector<double,block_size> > subgridh1functionalassembler(function,grid);
        H1FunctionalAssembler<HostGridType, typename HostBasis::LocalFiniteElement, Dune::FieldVector<double,block_size> > h1functionalassembler(function);

        /* assemble h1 functionals */
        assembler.assemble(subgridh1functionalassembler, g);
        hostassembler.assemble(h1functionalassembler, ghost);

        transfer.mtv(ghost,gsubcoeffs);

        assert(gsubcoeffs.size()==g.size());

        for (size_t j=0; j<basis.size(); ++j)
        {
            for (int k=0; k<block_size; ++k)
            {
                if (g[j][k]-gsubcoeffs[j][k]>1e-14)
                {
                    std::cout << "Problem detected: g[" << j << "," << k << "]= " << g[j][k] << "  gsubcoeffs[" << j << "," << k << "]= " << gsubcoeffs[j][k] << std::endl;;
                    return false;
                }
            }
        }
#endif
        return true;
    }
};

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    std::cout << "This is the SubgridXYFunctionalAssemblerTest" << std::endl;

//    SubgridL2FunctionalAssemblerTestSuite l2tests;
    SubgridH1FunctionalAssemblerTestSuite h1tests;

    bool passed = true;

    std::cout << "Testing SubgridL2FunctionalAssembler" << std::endl;
#if HAVE_UG
//    passed = passed and checkWithAdaptiveGrid<Dune::UGGrid<2> >(l2tests, 3, 3);
//    passed = passed and checkWithAdaptiveGrid<Dune::UGGrid<3> >(l2tests, 1, 2);
#endif
#if HAVE_DUNE_ALUGRID
//    passed = passed and checkWithAdaptiveGrid<Dune::ALUGrid<2,2, Dune::simplex, Dune::nonconforming> >(l2tests, 3, 3);
//    passed = passed and checkWithAdaptiveGrid<Dune::ALUGrid<3,3, Dune::simplex, Dune::nonconforming> >(l2tests, 1, 2);
#endif
    std::cout << "Testing SubgridH1FunctionalAssembler" << std::endl;
#if HAVE_UG
    passed = passed and checkWithAdaptiveGrid<Dune::UGGrid<2> >(h1tests, 3, 3);
    passed = passed and checkWithAdaptiveGrid<Dune::UGGrid<3> >(h1tests, 1, 2);
#endif
#if HAVE_DUNE_ALUGRID
    passed = passed and checkWithAdaptiveGrid<Dune::ALUGrid<2,2, Dune::simplex, Dune::nonconforming> >(h1tests, 3, 3);
//    passed = passed and checkWithAdaptiveGrid<Dune::ALUGrid<3,3, Dune::simplex, Dune::nonconforming> >(h1tests, 1, 2);
#endif

    return passed ? 0 : 1;

}
