#include "config.h"

/** \file
 *  \brief Unit tests for the various p?NodalBasis classes
 */

#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/onedgrid.hh>
//#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/utility/structuredgridfactory.hh>
#include <doc/grids/gridfactory/hybridtestgrids.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/fufem/functionspacebases/p1nodalbasis.hh>
#include <dune/fufem/functionspacebases/p2nodalbasis.hh>

#include "common.hh"

using namespace Dune;

/** \brief Coordinate function in one variable, constant in the others

    This is used to extract the positions of the Lagrange nodes.
 */
template <int dim>
struct CoordinateFunction
    : public Dune::VirtualFunction<FieldVector<double,dim>, FieldVector<double,1> >
{
    CoordinateFunction(int d)
    : d_(d)
    {}

    void evaluate(const FieldVector<double, dim>& x, FieldVector<double,1>& out) const {
        out[0] = x[d_];
    }

    int d_;
};


struct FunctionSpaceBasisTestSuite
{
    template <class NodalBasisType>
    bool testNodalBasis(const NodalBasisType& nodalBasis)
    {
        bool passed = true;
        std::cout << "Testing " << className<NodalBasisType>() << std::endl;

        typedef typename NodalBasisType::LocalFiniteElement LocalFiniteElementType;
        typedef typename NodalBasisType::GridView GridView;
        static const int dim = GridView::dimension;

        const GridView& gridView = nodalBasis.getGridView();

        // map that stores the world position of each Lagrange node indexed
        // by the corresponding global index
        typedef std::map<int, FieldVector<double,dim> > IndexMapType;
        IndexMapType indexMap;

        for (typename GridView::template Codim<0>::Iterator eIt=gridView.template begin<0>();
             eIt!=gridView.template end<0>();
             ++eIt) {
            const LocalFiniteElementType& lFE = nodalBasis.getLocalFiniteElement(*eIt);

            // Get the position for each Lagrange node
            std::vector<FieldVector<double,dim> > lagrangeNodes(lFE.localBasis().size());

            for (int i=0; i<dim; i++) {
                CoordinateFunction<dim> lFunction(i);
                std::vector<FieldVector<double,1> > coordinates;
                lFE.localInterpolation().interpolate(lFunction, coordinates);

                for (size_t j=0; j<coordinates.size(); j++)
                    lagrangeNodes[j][i] = coordinates[j];

            }

            // Look whether for each local dof the global position of its Lagrange node
            // and the global index reported by the nodal basis are consistent.
            for (size_t i=0; i<lFE.localBasis().size(); i++) {

                // world position of the Lagrange node
                FieldVector<double,dim> worldPos = eIt->geometry().global(lagrangeNodes[i]);

                // global index
                size_t idx = nodalBasis.index(*eIt, i);

                typename IndexMapType::iterator brother = indexMap.find(idx);
                if (brother == indexMap.end()) {
                    indexMap.insert(std::make_pair(idx, worldPos));
                } else {
                    double dist = (brother->second - worldPos).two_norm();
                    passed = passed and dist < 1e-4;
                }

            }
        }

        // At this point the index map must contain each Lagrange node exactly once.
        passed = passed and indexMap.size() == nodalBasis.size();
        return passed;
    }

    template <class GridType>
    bool check(const GridType& grid)
    {
        bool passed = true;
        typedef typename GridType::LeafGridView LeafView;
        const LeafView &leafView = grid.leafGridView();

        // Test the first-order basis
        P1NodalBasis<LeafView> p1NodalBasis(leafView);
        passed = passed and testNodalBasis(p1NodalBasis);

        // Test the second-order basis
        P2NodalBasis<LeafView> p2NodalBasis(leafView);
        passed = passed and testNodalBasis(p2NodalBasis);

        DuneFunctionsBasis<typename Functions::LagrangeBasis<LeafView,1> > fufemFunctionsPQk1NodalBasis(leafView);
        passed = passed and testNodalBasis(fufemFunctionsPQk1NodalBasis);

        DuneFunctionsBasis<typename Functions::LagrangeBasis<LeafView,2> > fufemFunctionsPQk2NodalBasis(leafView);
        passed = passed and testNodalBasis(fufemFunctionsPQk2NodalBasis);

        try {
            DuneFunctionsBasis<typename Functions::LagrangeBasis<LeafView,3> > fufemFunctionsPQk3NodalBasis(leafView);
            passed = passed and testNodalBasis(fufemFunctionsPQk3NodalBasis);
        } catch (Dune::NotImplemented e) {
          std::cout << e << std::endl;
        }

        return passed;
    }
};


int main(int argc, char* argv[])
{
    bool passed = true;
    Dune::MPIHelper::instance(argc, argv);

    FunctionSpaceBasisTestSuite testsuite;

    // Create a 1d test grid
    OneDGrid grid1d(5, 0.0, 1.0);

    passed = passed and testsuite.check(grid1d);

    passed = passed and checkWithStandardAdaptiveGrids(testsuite);
    passed = passed and checkWithStandardStructuredGrids(testsuite);

#if 0
    // Create a 2d test grid
    typedef UGGrid<2> GridType;
    //std::auto_ptr<GridType> grid2d(make2DHybridTestGrid<GridType>());

    array<unsigned int,2> elements2d;
    elements2d.fill(4);
    shared_ptr<GridType> grid2d = StructuredGridFactory<GridType>::createSimplexGrid(FieldVector<double,2>(0),
                                                                                     FieldVector<double,2>(1),
                                                                                     elements2d);

    passed = passed and testAllOrders(grid2d->leafGridView());

    // Create a 3d test grid
    array<unsigned int,3> elements3d;
    elements3d.fill(4);
    shared_ptr<UGGrid<3> > grid3d = StructuredGridFactory<UGGrid<3> >::createSimplexGrid(FieldVector<double,3>(0),
                                                                                         FieldVector<double,3>(1),
                                                                                         elements3d);

    passed = passed and testAllOrders(grid3d->leafGridView());
#endif
    return passed ? 0 : 1;
}
