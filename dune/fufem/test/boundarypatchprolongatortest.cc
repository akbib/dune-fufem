// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#include <config.h>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/fufem/boundarypatchprolongator.hh>

#include <dune/fufem/test/common.hh>



struct BoundaryPatchProlongatorTestSuite
{

    //! Extract the part of the boundary with center[dim-1]<=0
    struct LowerFaceInsertion {

        template <class Intersection>
        bool operator() (const Intersection& it) const {

            return it.geometry().center()[Intersection::dimension-1]<=0;
        }
    };

    template<class GridType>
    static bool checkBoundaryPatchProlongation(const GridType& grid)
    {
        bool passed = true;

        typedef BoundaryPatch<typename GridType::LevelGridView> LevelBP;
        typedef BoundaryPatch<typename GridType::LeafGridView> LeafBP;

        // make coarse level boundary patch
        LevelBP boundary(grid.levelGridView(0));
        boundary.insertFacesByProperty(LowerFaceInsertion());

        // prolong boundary patch to leaf grid
        LeafBP prolongedLeafBoundary;
        BoundaryPatchProlongator<GridType>::prolong(boundary,prolongedLeafBoundary);

        // create the same leaf patch directly
        LeafBP leafBoundary(grid.leafGridView());
        leafBoundary.insertFacesByProperty(LowerFaceInsertion());

        // Check if patches are the same
        for (const auto& i : leafBoundary)
            passed = passed and prolongedLeafBoundary.contains(i);

        for (const auto& i : prolongedLeafBoundary)
            passed = passed and leafBoundary.contains(i);

        return passed;
    }

    template<class GridType>
    bool check(const GridType& grid)
    {
        bool passed = true;

        passed = passed and checkBoundaryPatchProlongation<GridType>(grid);

        return passed;
    }
};


int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    BoundaryPatchProlongatorTestSuite tests;

    bool passed = checkWithStandardAdaptiveGrids(tests);

    return passed ? 0 : 1;
}
