// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_TEST_COMMON_HH
#define DUNE_FUFEM_TEST_COMMON_HH

#include <array>
#include <vector>
#include <dune/common/fvector.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/common/gridfactory.hh>

#if HAVE_UG
#include <dune/grid/uggrid.hh>
#include <dune/grid/uggrid/uggridfactory.hh>
#endif

#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#endif

#if HAVE_DUNE_SUBGRID
#include "dune/subgrid/subgrid.hh"
#endif


template <typename FT>
struct ToleranceTraits {
  static constexpr FT tol = 50 * std::numeric_limits<FT>::epsilon();
};

template <class T>
typename Dune::FieldTraits<typename T::field_type>::real_type diffDune(T t1, const T& t2)
{
  t1 -= t2;
  return t1.infinity_norm();
}

template <class TX>
bool isCloseDune(TX t1, const TX& t2)
{
  return diffDune(t1, t2) < ToleranceTraits<typename TX::field_type>::tol;
}

template <class ContainerType>
typename ContainerType::value_type diffSTL(const ContainerType &t1, const ContainerType& t2)
{
  typename ContainerType::value_type returnValue(0);
  auto t1It  = t1.begin();
  auto t2It  = t2.begin();
  auto t1End = t1.end();
  auto t2End = t2.end();
  for (; t1It!=t1End and t2It!=t2End; ++t1It, ++t2It)
      returnValue = std::max(std::abs(*t1It - *t2It),returnValue);

  return returnValue;
}

template <class ContainerType>
bool isCloseSTL(const ContainerType &t1, const ContainerType& t2)
{
  return diffSTL(t1, t2) < ToleranceTraits<typename ContainerType::value_type>::tol;
}

template <class FT, class Other>
bool isCloseAbs(FT t1, Other t2)
{
  return std::abs<FT>(t1 - t2) < ToleranceTraits<FT>::tol;
}

template<class GridView, class FunctionA, class FunctionB>
bool compareEvaluateByGridViewQuadrature(const FunctionA& fA, const FunctionB& fB, const GridView& gridView, int order)
{
    typedef typename FunctionA::RangeType RangeTypeA;
    typedef typename FunctionB::RangeType RangeTypeB;

    constexpr int dim = GridView::dimension;

    for(const auto& it : elements(gridView))
    {
        const auto& quad = Dune::QuadratureRules<typename GridView::ctype, dim>::rule(it.type(), order);
        const auto& geometry = it.geometry();
        for (const auto& pt : quad)
        {
            auto x = geometry.global(pt.position());
            RangeTypeA yA;
            RangeTypeB yB;

            fA.evaluate(x,yA);
            fB.evaluate(x,yB);
            yA -= yB;

            if (yA.infinity_norm() > 1e-12)
            {
                std::cout << "Result of evaluate() differs at global coordinate " << x << std::endl;
                return false;
            }
        }
    }
    return true;
}



template<class GridView, class FunctionA, class FunctionB>
bool compareEvaluateDerivativeByGridViewQuadrature(const FunctionA& fA, const FunctionB& fB, const GridView& gridView, int order)
{
    typedef typename FunctionA::DerivativeType DerivativeTypeA;
    typedef typename FunctionB::DerivativeType DerivativeTypeB;

    const int dim = GridView::Grid::dimension;

    for(const auto& it : elements(gridView))
    {
        const auto& quad = Dune::QuadratureRules<typename GridView::ctype, dim>::rule(it.type(), order);
        const auto& geometry = it.geometry();
        for (const auto& pt : quad)
        {
            auto x = geometry.global(pt.position());
            DerivativeTypeA dA;
            DerivativeTypeB dB;

            fA.evaluateDerivative(x, dA);
            fB.evaluateDerivative(x, dB);
            dA -= dB;

            if (dA.infinity_norm() > 1e-12)
            {
                std::cout << "Result of evaluateDerivative() differs at global coordinate " << x << std::endl;
                return false;
            }
        }
    }
    return true;;
}



template<class GridView, class FunctionA, class FunctionB>
bool compareEvaluateLocalByGridViewQuadrature(const FunctionA& fA, const FunctionB& fB, const GridView& gridView, int order)
{
    typedef typename FunctionA::RangeType RangeTypeA;
    typedef typename FunctionB::RangeType RangeTypeB;

    const int dim = GridView::Grid::dimension;

    for(const auto& it : elements(gridView))
    {
        if (not(fA.isDefinedOn(it)))
        {
            std::cout << "First function s not defined on element " << gridView.indexSet().index(it) << std::endl;
            return false;
        }
        if (not(fB.isDefinedOn(it)))
        {
            std::cout << "Second function s not defined on element " << gridView.indexSet().index(it) << std::endl;
            return false;
        }

        const auto& quad = Dune::QuadratureRules<typename GridView::ctype, dim>::rule(it.type(), order);
        for (const auto& pt : quad)
        {
            RangeTypeA yA;
            RangeTypeB yB;

            fA.evaluateLocal(it, pt.position(), yA);
            fB.evaluateLocal(it, pt.position(), yB);
            yA -= yB;

            if (yA.infinity_norm() > 1e-12)
            {
                std::cout << "Result of evaluateLocal() differs at local coordinate ";
                std::cout << pt.position();
                std::cout << " in element " << gridView.indexSet().index(it) << std::endl;
                return false;
            }
        }
    }
    return true;
}



template<class GridView, class FunctionA, class FunctionB>
bool compareEvaluateDerivativeLocalByGridViewQuadrature(const FunctionA& fA, const FunctionB& fB, const GridView& gridView, int order)
{
    typedef typename FunctionA::DerivativeType DerivativeTypeA;
    typedef typename FunctionB::DerivativeType DerivativeTypeB;

    const int dim = GridView::Grid::dimension;

    for(const auto& it : elements(gridView))
    {
        if (not(fA.isDefinedOn(it)))
        {
            std::cout << "First function s not defined on element " << gridView.indexSet().index(it) << std::endl;
            return false;
        }
        if (not(fB.isDefinedOn(it)))
        {
            std::cout << "Second function s not defined on element " << gridView.indexSet().index(it) << std::endl;
            return false;
        }

        const auto& quad = Dune::QuadratureRules<typename GridView::ctype, dim>::rule(it.type(), order);
        for (const auto& pt : quad)
        {
            DerivativeTypeA dA;
            DerivativeTypeB dB;

            fA.evaluateDerivativeLocal(it, pt.position(), dA);
            fB.evaluateDerivativeLocal(it, pt.position(), dB);
            dA -= dB;

            if (dA.infinity_norm() > 1e-12)
            {
                std::cout << "Result of evaluateDerivativeLocal() differs at local coordinate ";
                std::cout << pt.position();
                std::cout << " in element " << gridView.indexSet().index(it) << std::endl;
                return false;
            }
        }
    }
    return true;
}



template<class GridType>
std::unique_ptr<GridType> constructCoarseGrid()
{
    constexpr int dim = GridType::dimension;
    Dune::GridFactory<GridType> gridFactory;

    Dune::FieldVector<typename GridType::ctype, dim> pos(0);
    gridFactory.insertVertex(pos);
    for (int i=0; i<dim; i++) {
        pos = 0;   pos[i] = 1;
        gridFactory.insertVertex(pos);
    }

    std::vector<unsigned int> element(dim+1);
    for (int i=0; i<dim+1; i++)
        element[i] = i;

    gridFactory.insertElement( Dune::GeometryTypes::simplex(dim), element);

    return gridFactory.createGrid();
}

template<int dim>
Dune::YaspGrid<dim>* constructCoarseYaspGrid()
{
    typedef Dune::YaspGrid<dim> GridType;

    Dune::FieldVector<double, dim> L(1.0);
    std::array<int,dim> s;
    std::fill(s.begin(), s.end(), 1);

    GridType* grid = new GridType(L,s);

    return grid;
}


template<class GridType>
void refineLocally(GridType& grid, int refine=3)
{
    typedef typename GridType::LevelGridView GridView;

    for(int i=0; i<refine; ++i)
    {
        GridView gv = grid.levelGridView(grid.maxLevel());
        std::size_t half = gv.size(0)/2 + 1;

        for(const auto& it : elements(gv))
        {
            if ((std::size_t)gv.indexSet().index(it) < half)
                grid.mark(1, it);
        }
        grid.preAdapt();
        grid.adapt();
        grid.postAdapt();
    }
}

template<class GridType, class TestSuite>
bool checkWithAdaptiveGrid(TestSuite& suite, int uniformRefinements, int localRefinements)
{
    std::unique_ptr<GridType> gridPtr(constructCoarseGrid<GridType>());
    GridType& grid = *gridPtr;

    grid.globalRefine(uniformRefinements);
    refineLocally(grid, localRefinements);

    std::cout << "Running test on " << Dune::className(grid) << " with " << grid.size(0) << " elements." << std::endl;

    bool passed = suite.check(grid);
    if (passed)
        std::cout << "All tests passed with " << Dune::className(grid) << "." << std::endl;
    return passed;
}

template<int dim, class TestSuite>
bool checkWithStructuredGrid(TestSuite& suite, int uniformRefinements)
{
    typedef Dune::YaspGrid<dim> GridType;
    GridType* grid(constructCoarseYaspGrid<dim>());

    grid->globalRefine(uniformRefinements);

    std::cout << "Running test on " << Dune::className(grid) << " with " << grid->size(0) << " elements." << std::endl;

    bool passed = suite.check(*grid);
    if (passed)
        std::cout << "All tests passed with " << Dune::className(grid) << "." << std::endl;

    delete grid;
    return passed;
}

#if HAVE_DUNE_SUBGRID
/**
 *  Runs check<SubGrid<dim,HostGridType> > of a given TestSuite with a subgrid that
 *      * covers the whole domain of the hostgrid
 *      * contains some hostgrid leafelements but...
 *      * ...does not necessarily contain all hostgrid leafelements, i.e. does not resolve the hostgrid
 *
 *  \tparam HostGridType the type of the hostgrid
 *  \tparam TestSuite the type of the testsuite to run
 *  \param suite the testsuite to run
 *  \param uniformRefinements number of uniform refinement steps on the hostgrid prior to subgrid creation
 *  \param localRefinements number of local refinement steps on the hostgrid prior to subgrid creation
 *  \param name of the GridType (not only HostGridType) to be printed on screen
 *  \param properSubset true: select approximately a quarter of the host grid leaf elements, false: select the full host grid leaf
 */
template<class HostGridType, class TestSuite>
bool checkWithSubGrid(TestSuite& suite, int uniformRefinements, int localRefinements, std::string name, bool properSubset = true)
{
    std::shared_ptr<HostGridType> gridPtr(constructCoarseGrid<HostGridType>());
    HostGridType& hostgrid = *gridPtr;

    hostgrid.globalRefine(uniformRefinements);
    refineLocally(hostgrid, localRefinements);

    Dune::SubGrid<HostGridType::dimension, HostGridType> grid(hostgrid);

    if(properSubset) {
      grid.createBegin();
      /* For test purposes we don't want the subgrid to resolve the whole hostgrid... */
      grid.insertLevel(hostgrid.maxLevel()-1);
      /* ...but we do want it to contain some hostgrid leafelements */
      auto helt=hostgrid.leafGridView().template begin<0>();
      auto hend=hostgrid.leafGridView().template end<0>();
      for (int n_elts=0; helt!=hend and n_elts<hostgrid.size(0)/4; ++helt, ++n_elts)
        grid.insert(*helt);
      grid.createEnd();
      /* make sure these requirements are met */
      assert(grid.size(0)!=hostgrid.size(0));
    } else {
      grid.createBegin();
      grid.insertLevel(hostgrid.maxLevel());
      grid.createEnd();
    }

    std::cout << "Running test on " << name << " with " << grid.size(0) << " elements." << std::endl;

    bool passed = suite.check(grid);
    if (passed)
        std::cout << "All tests passed with " << name << "." << std::endl;
    return passed;
}
#endif

template<class TestSuite>
bool checkWithStandardAdaptiveGrids(TestSuite& suite)
{
    bool passed = true;

#if HAVE_UG
    passed = passed and checkWithAdaptiveGrid<Dune::UGGrid<2> >(suite, 3, 3);
// This is disabled due to a bug (https://gitlab.dune-project.org/core/dune-grid/issues/27)
// in parallel UGGrid. Once this is fixed we can reenable this in favour of the check
// with less refinement below.
//    passed = passed and checkWithAdaptiveGrid<Dune::UGGrid<3> >(suite, 1, 2);
    passed = passed and checkWithAdaptiveGrid<Dune::UGGrid<3> >(suite, 1, 1);
#if HAVE_DUNE_SUBGRID
    passed = passed and checkWithSubGrid<Dune::UGGrid<2> >(suite, 3, 3, "SubGrid< 2,UGGrid<2> >");
    passed = passed and checkWithSubGrid<Dune::UGGrid<3> >(suite, 1, 2, "SubGrid< 3,UGGrid<3> >");
#endif
#endif

#if HAVE_DUNE_ALUGRID
    passed = passed and checkWithAdaptiveGrid<Dune::ALUGrid<2, 2, Dune::simplex, Dune::nonconforming> >(suite, 3, 3);
    passed = passed and checkWithAdaptiveGrid<Dune::ALUGrid<3, 3, Dune::simplex, Dune::nonconforming> >(suite, 1, 2);
#if HAVE_DUNE_SUBGRID
    passed = passed and checkWithSubGrid<Dune::ALUGrid<2,2, Dune::simplex, Dune::nonconforming> >(suite, 3, 3, "SubGrid< 2,ALUGrid<...> >");
    passed = passed and checkWithSubGrid<Dune::ALUGrid<3,3, Dune::simplex, Dune::nonconforming> >(suite, 1, 2, "SubGrid< 3,ALUGrid<...> >");
#endif
#endif

    return passed;
}

template<class TestSuite>
bool checkWithStandardStructuredGrids(TestSuite& suite)
{
    bool passed = true;

    passed = passed and checkWithStructuredGrid<2>(suite, 4);
    passed = passed and checkWithStructuredGrid<3>(suite, 2);

    return passed;
}
#endif // #define DUNE_FUFEM_TEST_COMMON_HH
