#include <config.h>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/fufem/assemblers/basisinterpolationmatrixassembler.hh>

#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/fufem/functiontools/basisinterpolator.hh>
#include <dune/fufem/functionspacebases/p1nodalbasis.hh>
#include <dune/fufem/functionspacebases/p2nodalbasis.hh>
#include <dune/fufem/functionspacebases/p3nodalbasis.hh>
#include <dune/fufem/functionspacebases/refinedp1nodalbasis.hh>
#include <dune/fufem/functionspacebases/conformingbasis.hh>
#include "common.hh"

template<class DT, class RT>
class F:
    public Dune::VirtualFunction<DT, RT>
{
    public:
        void evaluate(const DT& x, RT& y) const
        {
            y = 1;
        }
};


struct Suite
{
    template<class GridType>
    bool check(const GridType& grid)
    {
        bool passed = true;

        typedef P1NodalBasis<typename GridType::LeafGridView> P1Basis;
        P1Basis p1Basis(grid.leafGridView());
        typedef P2NodalBasis<typename GridType::LeafGridView> P2Basis;
        P2Basis p2Basis(grid.leafGridView());
        typedef P3NodalBasis<typename GridType::LeafGridView> P3Basis;
        P3Basis p3Basis(grid.leafGridView());
        typedef RefinedP1NodalBasis<typename GridType::LeafGridView> RefinedP1Basis;
        RefinedP1Basis refP1Basis(grid.leafGridView());

        typedef ConformingBasis<P1Basis> ConfP1Basis;
        ConfP1Basis confP1Basis(p1Basis);
        typedef ConformingBasis<RefinedP1Basis> ConfRefinedP1Basis;
        ConfRefinedP1Basis confRefP1Basis(refP1Basis);

        std::cout<<"Checking Refined P1 -> P1 \n";
        passed = passed and checkInterpolation<P1Basis, RefinedP1Basis,3>(p1Basis,refP1Basis);

        if (!passed)
            return false;

        std::cout<<"Checking P2 -> P1 \n";
        passed = passed and checkInterpolation<P1Basis, P2Basis,2>(p1Basis,p2Basis);

        if (!passed)
            return false;

        std::cout<<"Checking P3 -> P1 \n";
        passed = passed and checkInterpolation<P1Basis, P3Basis,1>(p1Basis,p3Basis);

        if (!passed)
            return false;

        std::cout<<"Checking Conforming<P1> -> P1 \n";
        passed = passed and checkInterpolation<P1Basis,ConfP1Basis,3> (p1Basis, confP1Basis);

        if (!passed)
            return false;

        std::cout<<"Checking P1 -> Conforming<P1> \n";
        passed = passed and checkInterpolation<ConfP1Basis,P1Basis,3> (confP1Basis, p1Basis);

        if (!passed)
            return false;

        std::cout<<"Checking Conforming<RefinedP1> -> Conforming<P1> \n";
        passed = passed and checkInterpolation<ConfP1Basis,ConfRefinedP1Basis,1> (confP1Basis, confRefP1Basis);

        if (!passed)
            return false;

        std::cout<<"Checking P3 -> P2 \n";
        passed = passed and checkInterpolation<P2Basis, P3Basis,2>(p2Basis,p3Basis);

        return passed;
    }

    template<class BasisType0, class BasisType1, int ncomp>
    bool checkInterpolation(const BasisType0& coarseBase, const BasisType1& fineBase)
    {
        typedef Dune::FieldVector<double, BasisType0::GridView::dimensionworld> DT;
        typedef Dune::FieldVector<double, ncomp> RT;

        typedef Dune::BlockVector<RT> VectorType;
        typedef Dune::BCRSMatrix<Dune::FieldMatrix<double,ncomp,ncomp> > MatrixType;

        // Constant 1 function
        F<DT, RT> f;

        // interpolate in coarse basis
        VectorType v0;
        Functions::interpolate(coarseBase, v0, f);

        // interpolate from coarse to fine
        VectorType v1;
        Functions::interpolate(fineBase, v1, Functions::makeFunction(coarseBase, v0));

        // setup interpolation matrix
        MatrixType interpolMat;
        assembleBasisInterpolationMatrix(interpolMat, coarseBase, fineBase);

        // interpolate again
        VectorType v2(interpolMat.N());
        interpolMat.mv(v0,v2);


        for (size_t i=0; i<v1.size(); ++i)
            if ((v1[i]-v2[i]).two_norm()>1e-14)
                return false;

        return (v1.size() == v2.size());
    }

};

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    Suite tests;

    bool passed = checkWithStandardAdaptiveGrids(tests);

    return passed ? 0 : 1;
}
