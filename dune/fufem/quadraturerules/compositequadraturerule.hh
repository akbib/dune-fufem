#ifndef COMPOSITE_QUADRATURE_RULE_HH
#define COMPOSITE_QUADRATURE_RULE_HH

/** \file
 * \brief Construct composite quadrature rules from other quadrature rules
 */

#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/virtualrefinement.hh>


/** \brief Construct composite quadrature rules for simplex elements
 */
template <class ct, int dim>
class CompositeQuadratureRule:
    public Dune::QuadratureRule<ct,dim>
{
    public:
        /** \brief Construct composite quadrature rule
         * \param quad Base quadrature rule.  Element type of this rule must be simplex
         * \param refinement Number of uniform refinement steps
         */
        CompositeQuadratureRule(const Dune::QuadratureRule<ct,dim>& quad, int refinement)
        {
            
            typedef Dune::StaticRefinement<Dune::Impl::SimplexTopology<dim>::type::id, 
                                           ct,
                                           Dune::Impl::SimplexTopology<dim>::type::id,
                                           dim> Refinement;

            int numberOfSubelements = (1<<(dim*refinement));
            //ct volume = Dune::ReferenceElements<ct,dim>::general(quad.type()).volume()/numberOfSubelements;
            ct volumeFraction = ct(1.0)/numberOfSubelements;
            
            auto eSubEnd = Refinement::eEnd(Dune::refinementLevels(refinement));
            auto eSubIt  = Refinement::eBegin(Dune::refinementLevels(refinement));
            
            for (; eSubIt != eSubEnd; ++eSubIt) {
                
                for (size_t i=0; i<quad.size(); i++) {
                    
                    const Dune::FieldVector<ct,dim>& quadPos = quad[i].position();
             
                    this->push_back(Dune::QuadraturePoint<ct,dim>(eSubIt.geometry().global(quadPos), volumeFraction*quad[i].weight()));
                    
                }
                
            }
                
        }

};

#endif
