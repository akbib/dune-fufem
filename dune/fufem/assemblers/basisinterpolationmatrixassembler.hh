// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_ASSEMBLERS_BASIS_INTERPOLATION_MATRIX_ASSEMBLER_HH
#define DUNE_FUFEM_ASSEMBLERS_BASIS_INTERPOLATION_MATRIX_ASSEMBLER_HH

#include <type_traits>
#include <dune/common/bitsetvector.hh>

#include <dune/istl/matrixindexset.hh>
#include <dune/istl/scaledidmatrix.hh>

#include <dune/matrix-vector/addtodiagonal.hh>

#include <dune/fufem/functions/cachedcomponentwrapper.hh>
#include <dune/localfunctions/common/virtualinterface.hh>

/** \brief Wrapper that extracts a single local basis function. */
template<class LocalBasis, class Base>
class LocalBasisComponentWrapper :
    public CachedComponentWrapper<
        LocalBasisComponentWrapper<LocalBasis, Base>,
        std::vector<typename LocalBasis::Traits::RangeType>,
        Base>
{
  public:
    typedef std::vector<typename LocalBasis::Traits::RangeType> AllRangeType;

  private:
    typedef CachedComponentWrapper<LocalBasisComponentWrapper<LocalBasis, Base>, AllRangeType, Base> BaseClass;

  public:
    typedef typename Base::DomainType DomainType;
    typedef typename Base::RangeType RangeType;

        LocalBasisComponentWrapper(const LocalBasis& localBasis, int comp) :
            BaseClass(comp),
            localBasis_(localBasis)
        {}

        void evaluateAll(const DomainType& x, AllRangeType& y) const
        {
            localBasis_.evaluateFunction(x, y);
        }

    protected:
        const LocalBasis& localBasis_;
};

/** \brief Method that computes the interpolation matrix mapping coefficients from one basis to
 *         the coefficients of another basis.
 *         Note that the interpolation only works exactly if one basis can be represented by the other.
 *         (Here denoted by coarse and fine basis)
 *         In this case multiplication of the matrix from the left represents the interpolation.
 */
template<class MatrixType, class BasisType0, class BasisType1>
static void assembleBasisInterpolationMatrix(MatrixType& matrix, const BasisType0& coarseBasis, const BasisType1& fineBasis)
{
    typedef typename BasisType0::GridView GridView;

    static_assert(std::is_same<GridView, typename BasisType1::GridView>::value, "GridView's don't match!");

    const GridView& gridView = coarseBasis.getGridView();

    int rows = fineBasis.size();
    int cols = coarseBasis.size();

    typedef typename BasisType0::LocalFiniteElement FEType0;
    typedef typename BasisType1::LocalFiniteElement FEType1;
    typedef typename Dune::LocalFiniteElementFunctionBase<FEType0>::type FunctionBaseClass;
    typedef LocalBasisComponentWrapper<typename FEType0::Traits::LocalBasisType, FunctionBaseClass> LocalBasisWrapper;

    matrix.setSize(rows,cols);
    matrix.setBuildMode(MatrixType::random);

    auto eIt    = gridView.template begin<0>();
    auto eEndIt = gridView.template end<0>();

    // ///////////////////////////////////////////
    // Determine the indices present in the matrix
    // /////////////////////////////////////////////////

    // Only handle every dof once
    Dune::BitSetVector<1> processed(fineBasis.size());
    for (size_t i=0; i<processed.size();i++)
        if (fineBasis.isConstrained(i))
            processed[i] = true;

    Dune::MatrixIndexSet indices(rows, cols);

    for (; eIt != eEndIt; ++eIt) {

        // Get local finite elements
        const FEType0& lfe0 = coarseBasis.getLocalFiniteElement(*eIt);
        const FEType1& lfe1 = fineBasis.getLocalFiniteElement(*eIt);

        const size_t numBaseFct0 = lfe0.localBasis().size();
        const size_t numBaseFct1 = lfe1.localBasis().size();

        // check if all components have already been processed
        bool allProcessed = true;
        for(size_t i=0; i<numBaseFct1; ++i)
            allProcessed = allProcessed and processed[fineBasis.index(*eIt, i)][0];

        // preallocate vector for function evaluations
        std::vector<typename FEType0::Traits::LocalBasisType::Traits::RangeType> values(numBaseFct0);

        // Extract single basis functions into a format that can be used within local interpolation
        LocalBasisWrapper basisFctj(lfe0.localBasis(),0);

        for (size_t j=0; j<numBaseFct0; j++)
        {
            // wrap each local basis function as a local function.
            basisFctj.setIndex(j);

            // Interpolate j^th base function by the fine basis
            lfe1.localInterpolation().interpolate(basisFctj, values);

            int globalCoarse = coarseBasis.index(*eIt,j);

            for (size_t i=0; i<numBaseFct1; i++) {

                if (std::abs(values[i])<1e-12)
                    continue;

                int globalFine = fineBasis.index(*eIt,i);

                if (processed[globalFine][0])
                    continue;

                if (coarseBasis.isConstrained(globalCoarse)) {

                    const auto& lin = coarseBasis.constraints(globalCoarse);

                    for (size_t k=0; k<lin.size(); k++)
                        indices.add(globalFine, lin[k].index);
                } else
                    indices.add(globalFine, globalCoarse);
            }

        }

        for (size_t i=0; i<numBaseFct1; i++)
            processed[fineBasis.index(*eIt,i)]=true;
    }

    indices.exportIdx(matrix);
    matrix = 0;

    // /////////////////////////////////////////////
    // Compute the matrix
    // /////////////////////////////////////////////

    // Only handle every dof once
    processed.unsetAll();
    for (size_t i=0; i<processed.size();i++)
        if (fineBasis.isConstrained(i))
            processed[i] = true;

    eIt    = gridView.template begin<0>();
    for (; eIt != eEndIt; ++eIt) {

        // Get local finite element
        const FEType0& lfe0 = coarseBasis.getLocalFiniteElement(*eIt);
        const FEType1& lfe1 = fineBasis.getLocalFiniteElement(*eIt);

        const size_t numBaseFct0 = lfe0.localBasis().size();
        const size_t numBaseFct1 = lfe1.localBasis().size();

        // check if all components have already been processed
        bool allProcessed = true;
        for(size_t i=0; i<numBaseFct1; ++i)
            allProcessed = allProcessed and processed[fineBasis.index(*eIt, i)][0];

        if (not allProcessed) {

            // preallocate vector for function evaluations
            std::vector<typename FEType0::Traits::LocalBasisType::Traits::RangeType> values(numBaseFct0);

            // Extract single basis functions into a format that can be used within local interpolation
            LocalBasisWrapper basisFctj(lfe0.localBasis(),0);

            for (size_t j=0; j<numBaseFct0; j++)
            {
                // wrap each local basis function as a local function.
                basisFctj.setIndex(j);

                int globalCoarse = coarseBasis.index(*eIt,j);

                // Interpolate j^th base function by the fine basis
                lfe1.localInterpolation().interpolate(basisFctj, values);

                for (size_t i=0; i<numBaseFct1; i++) {

                    if (std::abs(values[i])<1e-12)
                        continue;

                    int globalFine = fineBasis.index(*eIt,i);
                    if (processed[globalFine][0])
                        continue;

                    if (coarseBasis.isConstrained(globalCoarse)) {

                        const auto& lin = coarseBasis.constraints(globalCoarse);

                        for (size_t k=0; k<lin.size(); k++)
                            Dune::MatrixVector::addToDiagonal(matrix[globalFine][lin[k].index],lin[k].factor*values[i]);
                    } else
                        Dune::MatrixVector::addToDiagonal(matrix[globalFine][globalCoarse],values[i]);
                }
            }

            for (size_t i=0; i<numBaseFct1; i++)
                    processed[fineBasis.index(*eIt,i)] = true;
        }
    }
}

#endif
