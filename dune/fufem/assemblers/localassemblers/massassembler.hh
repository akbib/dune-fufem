#ifndef MASS_ASSEMBLER_HH
#define MASS_ASSEMBLER_HH


#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/matrix.hh>

#include <dune/matrix-vector/addtodiagonal.hh>

#include "dune/fufem/quadraturerules/quadraturerulecache.hh"

#include "dune/fufem/assemblers/localoperatorassembler.hh"


//** \brief Local mass assembler **//
template <class GridType, class TrialLocalFE, class AnsatzLocalFE, class T=Dune::FieldMatrix<double,1,1> >
class MassAssembler : public LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE, T >
{
    private:
        typedef LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE ,T > Base;
        static const int dim = GridType::dimension;
        const int quadOrder_;


    public:
        typedef typename Base::Element Element;
        typedef typename Element::Geometry Geometry;
        typedef typename Base::BoolMatrix BoolMatrix;
        typedef typename Base::LocalMatrix LocalMatrix;

        MassAssembler():
            quadOrder_(-1)
        {}

        DUNE_DEPRECATED_MSG("Quadrature order is now selected automatically. you don't need to specify it anymore.")
        MassAssembler(int quadOrder):
            quadOrder_(quadOrder)
        {}

        void indices(const Element& element, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            isNonZero = true;
        }

        template <class BoundaryIterator>
        void indices(const BoundaryIterator& it, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            isNonZero = true;
        }

        void assemble(const Element& element, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            typedef typename Dune::template FieldVector<double,dim> FVdim;
            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::RangeType RangeType;

            // Make sure we got suitable shape functions
            assert(tFE.type() == element.type());
            assert(aFE.type() == element.type());

            // check if ansatz local fe = test local fe
//            if (not Base::isSameFE(tFE, aFE))
//                DUNE_THROW(Dune::NotImplemented, "MassAssembler is only implemented for ansatz space=test space!");

            int rows = localMatrix.N();
            int cols = localMatrix.M();

            // get geometry and store it
            const Geometry geometry = element.geometry();

            localMatrix = 0.0;

            // get quadrature rule
            QuadratureRuleKey quadKey = QuadratureRuleKey(tFE)
                .product(QuadratureRuleKey(aFE));
            if (quadOrder_>=0)
                quadKey.setOrder(quadOrder_);
            const Dune::template QuadratureRule<double, dim>& quad = QuadratureRuleCache<double, dim>::rule(quadKey);

            // store values of shape functions
            std::vector<RangeType> values(tFE.localBasis().size());

            // loop over quadrature points
            for (size_t pt=0; pt < quad.size(); ++pt)
            {
                // get quadrature point
                const FVdim& quadPos = quad[pt].position();

                // get integration factor
                const double integrationElement = geometry.integrationElement(quadPos);

                // evaluate basis functions
                tFE.localBasis().evaluateFunction(quadPos, values);

                // compute matrix entries
                double z = quad[pt].weight() * integrationElement;
                for(int i=0; i<rows; ++i)
                {
                    double zi = values[i]*z;

                    for (int j=i+1; j<cols; ++j)
                    {
                        double zij = values[j] * zi;
                        Dune::MatrixVector::addToDiagonal(localMatrix[i][j],zij);
                        Dune::MatrixVector::addToDiagonal(localMatrix[j][i],zij);
                    }
                    Dune::MatrixVector::addToDiagonal(localMatrix[i][i], values[i] * zi);
                }
            }
        }


        /** \brief Assemble the local mass matrix for a given boundary face
         */
        template <class BoundaryIterator>
        void assemble(const BoundaryIterator& it, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            typedef typename BoundaryIterator::Intersection::Geometry Geometry;
            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::RangeType RangeType;

            // Make sure we got suitable shape functions
            assert(tFE.type() == it->inside().type());
            assert(aFE.type() == it->inside().type());

            // check if ansatz local fe = test local fe
            if (not Base::isSameFE(tFE, aFE))
                DUNE_THROW(Dune::NotImplemented, "MassAssembler is only implemented for ansatz space=test space!");

            int rows = localMatrix.N();
            int cols = localMatrix.M();

            // get geometry and store it
            const Geometry intersectionGeometry = it->geometry();

            localMatrix = 0.0;

            // get quadrature rule
            QuadratureRuleKey tFEquad(it->type(), tFE.localBasis().order());
            QuadratureRuleKey quadKey = tFEquad.square();

            const Dune::template QuadratureRule<double, dim-1>& quad = QuadratureRuleCache<double, dim-1>::rule(quadKey);

            // store values of shape functions
            std::vector<RangeType> values(tFE.localBasis().size());

            // loop over quadrature points
            for (size_t pt=0; pt < quad.size(); ++pt)
            {
                // get quadrature point
                const Dune::FieldVector<double,dim-1>& quadPos = quad[pt].position();

                // get integration factor
                const double integrationElement = intersectionGeometry.integrationElement(quadPos);

                // get values of shape functions
                tFE.localBasis().evaluateFunction(it->geometryInInside().global(quadPos), values);

                // compute matrix entries
                double z = quad[pt].weight() * integrationElement;
                for (int i=0; i<rows; ++i)
                {
                    for (int j=i+1; j<cols; ++j)
                    {
                        double zij = values[i] * values[j] * z;
                        Dune::MatrixVector::addToDiagonal(localMatrix[i][j],zij);
                        Dune::MatrixVector::addToDiagonal(localMatrix[j][i],zij);
                    }
                    Dune::MatrixVector::addToDiagonal(localMatrix[i][i], values[i] * values[i] * z);
                }
            }

        }

};


#endif

