#ifndef ST_VENANT_KIRCHHOFF_ASSEMBLER_HH
#define ST_VENANT_KIRCHHOFF_ASSEMBLER_HH

#include "dune/fufem/assemblers/localassemblers/secondorderoperatorassembler.hh"
#include "dune/fufem/assemblers/localassemblers/strainproductassembler.hh"
#include "dune/fufem/symmetrictensor.hh"

#include <dune/functions/gridfunctions/gridviewfunction.hh>

template <class GradientVector, class LocalMatrix>
inline LocalMatrix
stVenantKirchhoffContraction(const GradientVector& g1, const GradientVector& g2,
                             typename GradientVector::field_type E,
                             typename GradientVector::field_type nu)
{
    using ctype = typename GradientVector::field_type;
    int const dim = GradientVector::dimension;
    /*
      To arrive at these expressions, run the following maxima program:

      dim : 3;
      rowvec(var) := matrix(makelist(var[i],i,1,dim));
      colvec(var) := apply(matrix,makelist([var[i]],i,dim));
      U : kronecker_product(rowvec(u),colvec(alpha));
      V : kronecker_product(rowvec(v),colvec(delta));
      strain(d) := (d + transpose(d))/2;
      stress(e) := L*mat_trace(e)*identfor(e) + 2*mu*e;
      contract(t1,t2) := sum(sum(t1[i][j]*t2[j][i],i,1,dim),j,1,dim);
      pro : contract(strain(U), stress(strain(V)));
      apply(facsum, [pro, flatten(makelist([u[i],v[i]],i,1,dim))]);
    */
    ctype const lambda = E * nu / (1.0+nu) / (1.0-2.0*nu);
    ctype const mu = 0.5 * E / (1+nu);
    LocalMatrix R = Dune::ScaledIdentityMatrix<ctype,dim>(mu*(g1*g2));
    for(std::size_t i=0; i<R.N(); ++i)
      for(std::size_t j=0; j<R.N(); ++j)
        R[i][j] += mu * (g1[j]*g2[i]) + lambda * (g1[i]*g2[j]);
    return R;
}

template <class Grid, class LFE, class LocalMatrix>
auto getStVenantKirchhoffAssembler(Grid const &grid, double E, double nu)
{
  using GlobalCoordinate = typename Grid::template Codim<0>::Geometry::GlobalCoordinate;
  // decltype(contraction) needs to be a lambda rather than a function pointer
  // in order to allow for it to be inlined
  auto const contraction = [](auto&&... args) {
    return stVenantKirchhoffContraction<GlobalCoordinate, LocalMatrix>(
        std::forward<decltype(args)>(args)...);
  };
  size_t const dim = GlobalCoordinate::dimension;
  auto const leafView = grid.leafGridView();

  auto funcE = [E](GlobalCoordinate const &) {return E;};
  auto funcNu = [nu](GlobalCoordinate const &) {return nu;};
  auto gridFuncE = Dune::Functions::makeGridViewFunction(funcE, leafView);
  auto gridFuncNu = Dune::Functions::makeGridViewFunction(funcNu, leafView);

  return SecondOrderOperatorAssembler<Grid, LFE, LFE, decltype(contraction),
                                      LocalMatrix, decltype(gridFuncE),
                                      decltype(gridFuncNu)>(
      contraction, true, QuadratureRuleKey(dim, 0), gridFuncE, gridFuncNu);
}

template <class Grid, class LFE, class LocalMatrix, class F1, class F2>
auto getStVenantKirchhoffAssembler(Grid const &grid, const F1& E, const F2& nu,
                                   QuadratureRuleKey quadratureRuleKey)
{
  using GlobalCoordinate = typename Grid::template Codim<0>::Geometry::GlobalCoordinate;
  // decltype(contraction) needs to be a lambda rather than a function pointer
  // in order to allow for it to be inlined
  auto const contraction = [](auto&&... args) {
    return stVenantKirchhoffContraction<GlobalCoordinate, LocalMatrix>(
        std::forward<decltype(args)>(args)...);
  };
  auto const leafView = grid.leafGridView();

  auto gridFuncE = Dune::Functions::makeGridViewFunction(E, leafView);
  auto gridFuncNu = Dune::Functions::makeGridViewFunction(nu, leafView);

  return SecondOrderOperatorAssembler<Grid, LFE, LFE, decltype(contraction),
                                      LocalMatrix, decltype(gridFuncE),
                                      decltype(gridFuncNu)>(
      contraction, true, quadratureRuleKey, gridFuncE, gridFuncNu);
}

/** \brief Assembler for a St.-Venant-Kirchhoff material (i.e., linear elasticity)
 */
template <class GridType, class TrialLocalFE, class AnsatzLocalFE>
class StVenantKirchhoffAssembler
    : public StrainProductAssembler< GridType, TrialLocalFE, AnsatzLocalFE, StVenantKirchhoffAssembler<GridType,TrialLocalFE,AnsatzLocalFE> >
{
    private:
        typedef StrainProductAssembler< GridType, TrialLocalFE, AnsatzLocalFE, StVenantKirchhoffAssembler<GridType,TrialLocalFE,AnsatzLocalFE> > SPA;
        using SPA::dim;
        using typename SPA::ctype;

        //! Lame's first parameter
        const ctype lambda_;
        //! Lame's second parameter (also known as G)
        const ctype twoMu_;

    public:
        using typename SPA::T;
        using typename SPA::BoolMatrix;
        using typename SPA::Element;
        using typename SPA::LocalMatrix;

    /**
     * \param E  Young's modulus
     * \param nu Poisson's ratio
     */
    StVenantKirchhoffAssembler(ctype E, ctype nu):
        lambda_(E * nu / (1.0+nu) / (1.0-2.0*nu) ),
        twoMu_(E / (1+nu) )
    {}

    SymmetricTensor<dim> strainToStress(const SymmetricTensor<dim>& strain,
                                        const Element&,
                                        const Dune::FieldVector<ctype, dim>&) const {
        SymmetricTensor<dim> stress = strain;
        stress *= twoMu_;
        stress.addToDiag(lambda_ * strain.trace());
        return stress;
      }
};


#endif
