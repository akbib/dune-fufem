// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUNCTIONS_COMMON_ISTL_BACKEND_HH
#define DUNE_FUNCTIONS_COMMON_ISTL_BACKEND_HH


#include <dune/common/indices.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>

#include <dune/functions/common/indexaccess.hh>
#include <dune/functions/functionspacebases/hierarchicvectorwrapper.hh>

#include <dune/matrix-vector/traits/scalartraits.hh>


namespace Dune {
namespace Fufem {



template<class Base>
class SingleRowMatrix :
  public Base
{
public:
  using Base::Base;
};

template<class Base>
class SingleColumnMatrix :
  public Base
{
public:
  using Base::Base;
};

template<class Base>
class SingleEntryMatrix :
  public SingleRowMatrix<SingleColumnMatrix<Base>>
{
public:
  using Base::Base;
};



namespace Imp {

constexpr std::false_type isSingleColumnMatrix(const void*)
{ return {}; }

template<class T>
constexpr std::true_type isSingleColumnMatrix(const SingleColumnMatrix<T>*)
{ return {}; }

constexpr std::false_type isSingleRowMatrix(const void*)
{ return {}; }

template<class T>
constexpr std::true_type isSingleRowMatrix(const SingleRowMatrix<T>*)
{ return {}; }

} // namespace Imp


/**
 * \brief Traits class to check if matrix is marked as single column matrix
 */
template<class T>
struct IsSingleColumnMatrix : public decltype(Imp::isSingleColumnMatrix( std::declval<typename std::decay<T>::type*>()))
{};

/**
 * \brief Traits class to check if matrix is marked as single row matrix
 */
template<class T>
struct IsSingleRowMatrix : public decltype(Imp::isSingleRowMatrix( std::declval<typename std::decay<T>::type*>()))
{};



/**
 * \brief Helper class for building matrix pattern
 *
 * This needs to be spezialized for the specific
 * matrix types.
 */
template<class Matrix>
class MatrixBuilder;



/**
 * \brief Helper class for building matrix pattern
 *
 * This spezialization for BCRSMatrix essentially
 * forwards to Dune::MatrixIndexSet.
 */
template<class T, class A>
class MatrixBuilder<Dune::BCRSMatrix<T, A>>
{
public:

  using Matrix = Dune::BCRSMatrix<T, A>;

  MatrixBuilder(Matrix& matrix) :
    matrix_(matrix)
  {}

  template<class RowSizeInfo, class ColSizeInfo>
  void resize(const RowSizeInfo& rowSizeInfo, const ColSizeInfo& colSizeInfo)
  {
    indices_.resize(rowSizeInfo.size(), colSizeInfo.size());
  }

  template<class RowIndex, class ColIndex>
  void insertEntry(const RowIndex& rowIndex, const ColIndex& colIndex)
  {
    indices_.add(rowIndex[0], colIndex[0]);
  }

  void setupMatrix()
  {
    indices_.exportIdx(matrix_);
  }

private:
  Dune::MatrixIndexSet indices_;
  Matrix& matrix_;
};



template<class M, class E=typename M::field_type>
class ISTLMatrixBackend
{

  template<class Result, class RowIndex, class ColIndex>
  struct MatrixMultiIndexResolver
  {
    MatrixMultiIndexResolver(const RowIndex& rowIndex, const ColIndex& colIndex) :
      rowIndex_(rowIndex),
      colIndex_(colIndex)
    {}

    template<class C>
    using isReturnable =
    std::integral_constant<bool,
      std::is_convertible<C&, Result>::value or
      Dune::MatrixVector::Traits::ScalarTraits<std::decay_t<C>>::isScalar
    >;

    template<class C>
    auto& toScalar(C&& c) {
      return std::forward<C>(c);
    }

    // Catch the cases where a FieldMatrix<K, 1, 1> was supplied
    auto& toScalar(FieldMatrix<std::decay_t<Result>, 1, 1>& c) {
      return c[0][0];
    }

    auto& toScalar(const FieldMatrix<std::decay_t<Result>, 1, 1>& c) {
      return c[0][0];
    }

    template<class C,
      typename std::enable_if<not isReturnable<C>::value, int>::type = 0,
      typename std::enable_if<IsSingleRowMatrix<C>::value, int>::type = 0>
    Result operator()(C&& c)
    {
      using namespace Dune::Indices;
      using namespace Dune::Functions;
      auto&& subRowIndex = rowIndex_;
      auto&& subColIndex = Dune::Functions::Imp::shiftedDynamicMultiIndex<1>(colIndex_);
      auto&& subIndexResolver = MatrixMultiIndexResolver<Result, decltype(subRowIndex), decltype(subColIndex)>(subRowIndex, subColIndex);
      return static_cast<Result>((hybridIndexAccess(c, _0, [&](auto&& ci) -> decltype(auto) {
        return static_cast<Result>((hybridIndexAccess(ci, colIndex_[_0], subIndexResolver)));
      })));
    }

    template<class C,
      typename std::enable_if<not isReturnable<C>::value, int>::type = 0,
      typename std::enable_if<IsSingleColumnMatrix<C>::value, int>::type = 0>
    Result operator()(C&& c)
    {
      using namespace Dune::Indices;
      using namespace Dune::Functions;
      auto&& subRowIndex = Dune::Functions::Imp::shiftedDynamicMultiIndex<1>(rowIndex_);
      auto&& subColIndex = colIndex_;
      auto&& subIndexResolver = MatrixMultiIndexResolver<Result, decltype(subRowIndex), decltype(subColIndex)>(subRowIndex, subColIndex);
      return static_cast<Result>((hybridIndexAccess(c, rowIndex_[_0], [&](auto&& ci) -> decltype(auto) {
        return static_cast<Result>((hybridIndexAccess(ci, _0, subIndexResolver)));
      })));
    }

    template<class C,
      typename std::enable_if<not isReturnable<C>::value, int>::type = 0,
      typename std::enable_if<not IsSingleRowMatrix<C>::value, int>::type = 0,
      typename std::enable_if<not IsSingleColumnMatrix<C>::value, int>::type = 0>
    Result operator()(C&& c)
    {
      using namespace Dune::Indices;
      using namespace Dune::Functions;
      auto&& subRowIndex = Dune::Functions::Imp::shiftedDynamicMultiIndex<1>(rowIndex_);
      auto&& subColIndex = Dune::Functions::Imp::shiftedDynamicMultiIndex<1>(colIndex_);
      auto&& subIndexResolver = MatrixMultiIndexResolver<Result, decltype(subRowIndex), decltype(subColIndex)>(subRowIndex, subColIndex);
      return static_cast<Result>((hybridIndexAccess(c, rowIndex_[_0], [&](auto&& ci) -> decltype(auto) {
        return static_cast<Result>((hybridIndexAccess(ci, colIndex_[_0], subIndexResolver)));
      })));
    }

    template<class C,
      typename std::enable_if<isReturnable<C>::value, int>::type = 0>
    Result operator()(C&& c)
    {
      return static_cast<Result>(toScalar(std::forward<C>(c)));
    }

    const RowIndex& rowIndex_;
    const ColIndex& colIndex_;
  };



public:

  using Matrix = M;
  using Entry = E;
  using value_type = Entry;

  ISTLMatrixBackend(Matrix& matrix) :
    matrix_(&matrix)
  {}

  MatrixBuilder<Matrix> patternBuilder()
  {
    return {*matrix_};
  }

  template<class RowMultiIndex, class ColMultiIndex>
  const Entry& operator()(const RowMultiIndex& row, const ColMultiIndex& col) const
  {
    MatrixMultiIndexResolver<const Entry&, RowMultiIndex, ColMultiIndex> i(row, col);
    return i(*matrix_);
  }

  template<class RowMultiIndex, class ColMultiIndex>
  Entry& operator()(const RowMultiIndex& row, const ColMultiIndex& col)
  {
    MatrixMultiIndexResolver<Entry&, RowMultiIndex, ColMultiIndex> i(row, col);
    return i(*matrix_);
  }

protected:

  Matrix* matrix_;
};



template<class Entry, class Matrix>
auto istlMatrixBackend(Matrix& matrix)
{
  return ISTLMatrixBackend<Matrix, Entry>(matrix);
}

template<class Matrix>
auto istlMatrixBackend(Matrix& matrix)
{
  return ISTLMatrixBackend<Matrix, typename Matrix::field_type>(matrix);
}



template<class Entry, class Vector>
auto istlVectorBackend(Vector& vector)
{
  return Dune::Functions::HierarchicVectorWrapper<Vector, Entry>(vector);
}

template<class Vector>
auto istlVectorBackend(Vector& vector)
{
  return Dune::Functions::HierarchicVectorWrapper<Vector, typename Vector::field_type>(vector);
}



} // namespace Dune::Fufem
} // namespace Dune



#endif // DUNE_FUNCTIONS_COMMON_ISTL_BACKEND_HH
