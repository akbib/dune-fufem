// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_ASSEMBLERS_LOCAL_FUNCTIONAL_ASSEMBLER_HH
#define DUNE_FUFEM_ASSEMBLERS_LOCAL_FUNCTIONAL_ASSEMBLER_HH

#include <dune/istl/bvector.hh>

/** \brief Abstract base class for local operator assemblers
 *
 *  \tparam GridType The grid we are assembling on
 *  \tparam TrialLocalFE the local finite element of the trial space
 *  \tparam AnsatzLocalFE the local finite element of the ansatz space
 *  \tparam T the block type
 */
template <class GridType, class TrialLocalFE, typename T>
class LocalFunctionalAssembler {

    public:
        using Element = typename GridType::template Codim<0>::Entity;
        using LocalVector = Dune::BlockVector<T>;

        /** \brief Assemble a local problem
         *
         *  \param element the grid element on which to operate
         *  \param localVector will contain the assembled element vector
         *  \param tFE the local finite element in the trial space used on element
         */
        virtual void assemble(const Element& element, LocalVector& localVector,
                              const TrialLocalFE& tFE) const  = 0;

        template<class TrialLocalView>
        void operator()(
                const Element& element,
                LocalVector& localVector,
                const TrialLocalView& trialLocalView) const
        {
            assemble(element, localVector, trialLocalView.tree().finiteElement());
        }



};

#endif

