#ifndef Q1_NODALBASIS_HH
#define Q1_NODALBASIS_HH

/*
#warning This file is deprecated.  All implementations of function space bases in dune-fufem \
  are in the process of being replaced by counterparts in the new dune-functions module. \
  Those are syntactically different, but semantically very close to the dune-fufem implementations. \
  To get rid of this warning, replace all occurrences of the Q1NodalBasis<...> class in your code \
  by DuneFunctionsBasis<Dune::Functions::LagrangeBasis<...,1> >.
*/

/**
   @file
   @brief First-order Lagrangian nodal basis on hypercube grids

   @author
 */

#include <dune/geometry/type.hh>

#include <dune/localfunctions/lagrange/q1.hh>


#include <dune/fufem/functionspacebases/functionspacebasis.hh>



template <class GV, class RT=double>
class Q1NodalBasis :
    public FunctionSpaceBasis<GV, RT,
        typename Dune::template Q1LocalFiniteElement<typename GV::Grid::ctype, RT, GV::Grid::dimension> >
{
    protected:
        typedef typename Dune::template Q1LocalFiniteElement<typename GV::Grid::ctype, RT, GV::Grid::dimension> LFE;
        typedef FunctionSpaceBasis<GV, RT, LFE> Base;
        typedef typename Base::Element Element;

        using Base::dim;
        using Base::gridview_;


    public:
        typedef typename Base::GridView GridView;
        typedef typename Base::ReturnType ReturnType;
        typedef typename Base::LocalFiniteElement LocalFiniteElement;
        typedef typename Base::LinearCombination LinearCombination;

        Q1NodalBasis(const GridView& gridview) :
            Base(gridview),
            localFE_()
        {}

        size_t size() const
        {
            return gridview_.indexSet().size(dim);
        }

        const LocalFiniteElement& getLocalFiniteElement(const Element& e DUNE_UNUSED) const
        {
            return localFE_;
        }

        int index(const Element& e, const int i) const
        {
            return gridview_.indexSet().template subIndex(e, localFE_.localCoefficients().localKey(i).subEntity(), dim);
        }

    protected:
        const LocalFiniteElement localFE_;
};

#endif

