#ifndef DUNE_FUFEM_FUNCTIONSPACEBASES_DUNEFUNCTIONSBASIS_HH
#define DUNE_FUFEM_FUNCTIONSPACEBASES_DUNEFUNCTIONSBASIS_HH

/**
   @file
   @brief A wrapper around the function space bases from the dune-functions module

   @author Oliver Sander
 */

#include <dune/common/version.hh>

#include <dune/fufem/functionspacebases/functionspacebasis.hh>


/** \brief Wrap a basis from dune-functions and make it look like a dune-fufem basis
 *
 * \tparam A function space basis from dune-functions
 *
 * This class exists for transition purposes only.  In the long run, we want to use the function
 * space bases from dune-functions directly in dune-fufem.  However, to get some experience with
 * them, we start with this wrapper.
 *
 * Using this wrapper may be a bit slower than using either the dune-fufem or the dune-functions
 * bases directly, because 'bind' needs to be called at each access to 'getLocalFiniteElement'
 * and 'index'.  I don't know whether the difference is significant.
 */
template <class DFBasis>
class DuneFunctionsBasis
: public FunctionSpaceBasis<typename DFBasis::GridView,
                            double,
                            typename DFBasis::LocalView::Tree::FiniteElement >
{
protected:
  typedef FunctionSpaceBasis<typename DFBasis::GridView,double,typename DFBasis::LocalView::Tree::FiniteElement> Base;

  typedef typename Base::Element Element;
public:
  /** \brief GridView on which this basis is defined */
  typedef typename DFBasis::GridView GridView;

  /** \brief Number type used for basis function values */
  typedef typename Base::ReturnType ReturnType;

  /** \brief The LocalFiniteElement (in the dune-localfunctions sense of the word)
   *  that implements the local basis on each element
   */
  typedef typename DFBasis::LocalView::Tree::FiniteElement LocalFiniteElement;

  /** \brief Constructor from a dune-functions basis */
  DuneFunctionsBasis(DFBasis dfBasis)
  : Base(dfBasis.gridView()),
    dfBasis_(std::move(dfBasis)),
    localView_(dfBasis_.localView())
  {}

  /** \brief Return the total number of basis vectors in this basis
   */
  size_t size() const
  {
    return dfBasis_.size();
  }

  /** \brief Get a local finite element (i.e., a set of shape functions) for the given element
   */
  const LocalFiniteElement& getLocalFiniteElement(const Element& element) const
  {
    localView_.bind(element);
    return localView_.tree().finiteElement();
  }

  /** \brief Get the global index of the i-th local degree of freedom on element 'element'
   */
  int index(const Element& element, const int i) const
  {
#if DUNE_VERSION_LT(DUNE_FUNCTIONS,2,7)
    auto localIndexSet = dfBasis_.localIndexSet();

    localView_.bind(element);
    localIndexSet.bind(localView_);

    return localIndexSet.index(i)[0];
#else
    localView_.bind(element);
    return localView_.index(i)[0];
#endif
  }

protected:

  // The dune-functions basis we are wrapping
  const DFBasis dfBasis_;

  // Must be mutable, because it gets bound to individual elements during the const methods
  // 'getLocalFiniteElement' and 'index'.
  mutable typename DFBasis::LocalView localView_;
};

#endif

