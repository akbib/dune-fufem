#ifndef REFINED_P1_NODALBASIS_HH
#define REFINED_P1_NODALBASIS_HH

/**
   @file
   @brief

   @author
 */

#include <dune/localfunctions/refined/refinedp1.hh>

#include <dune/fufem/functionspacebases/functionspacebasis.hh>
#include <dune/fufem/functionspacebases/p2nodalbasis.hh>



template <class GV, class RT=double>
class RefinedP1NodalBasis :
    public FunctionSpaceBasis<
        GV,
        RT,
        typename Dune::template RefinedP1LocalFiniteElement<typename GV::Grid::ctype, RT, GV::dimension> >
{
    protected:
        typedef typename GV::Grid::ctype ctype;

        typedef typename Dune::template RefinedP1LocalFiniteElement<ctype, RT, GV::dimension> LFE;

        typedef FunctionSpaceBasis<GV, RT, LFE> Base;
        typedef typename Base::Element Element;

        using Base::dim;
        using Base::gridview_;

    public:
        typedef typename Base::GridView GridView;
        typedef typename Base::ReturnType ReturnType;
        typedef typename Base::LocalFiniteElement LocalFiniteElement;
        typedef typename Base::LinearCombination LinearCombination;

        RefinedP1NodalBasis(const GridView& gridview) :
            Base(gridview),
            mapper_(gridview)
        {}

        size_t size() const
        {
            return mapper_.size();
        }

        void update(const GridView& gridview)
        {
            Base::update(gridview);
            mapper_.update();
        }

        const LocalFiniteElement& getLocalFiniteElement(const Element& e DUNE_UNUSED) const
        {
            return fe_;
        }

        int index(const Element& e, const int i) const
        {
            return mapper_.index(e, fe_.localCoefficients().localKey(i));
        }

    protected:
        LFE fe_;
        P2BasisMapper<GV> mapper_;
};

#endif

