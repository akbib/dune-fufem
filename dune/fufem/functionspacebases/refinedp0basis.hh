#ifndef REFINED_P0_BASIS_HH
#define REFINED_P0_BASIS_HH

/**
   @file
   @brief

   @author
 */

#include <dune/localfunctions/refined/refinedp0.hh>

#include <dune/fufem/functionspacebases/functionspacebasis.hh>



template <class GV, class RT=double>
class RefinedP0Basis :
    public FunctionSpaceBasis<
        GV,
        RT,
        typename Dune::template RefinedP0LocalFiniteElement<typename GV::Grid::ctype, RT, GV::dimension> >
{
    protected:
        typedef typename GV::Grid::ctype ctype;

        typedef typename Dune::template RefinedP0LocalFiniteElement<ctype, RT, GV::dimension> LFE;

        typedef FunctionSpaceBasis<GV, RT, LFE> Base;
        typedef typename Base::Element Element;

        using Base::dim;
        using Base::gridview_;

        typedef typename Dune::MultipleCodimMultipleGeomTypeMapper<GV, Dune::MCMGElementLayout > P0BasisMapper;

    public:
        typedef typename Base::GridView GridView;
        typedef typename Base::ReturnType ReturnType;
        typedef typename Base::LocalFiniteElement LocalFiniteElement;
        typedef typename Base::LinearCombination LinearCombination;

        RefinedP0Basis(const GridView& gridview) :
            Base(gridview),
            mapper_(gridview)
        {}

        void update(const GridView& gridview)
        {
            gridview_ = gridview;
            mapper_.update();
        }

        size_t size() const
        {
            return mapper_.size() * localFE_.localBasis().size();
        }

        const LocalFiniteElement& getLocalFiniteElement(const Element& e) const
        {
            return localFE_;
        }

        int index(const Element& e, const int i) const
        {
            return (mapper_.index(e) * localFE_.localBasis().size()) + i;
        }

    protected:
        const LocalFiniteElement localFE_;
        P0BasisMapper mapper_;
};

#endif
