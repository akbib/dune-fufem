#ifndef BOUNDINGBOX_HH
#define BOUNDINGBOX_HH

#include <vector>

#include "box.hh"
#include <dune/common/exceptions.hh>

/** \brief Class for bounding boxes of point clouds
 *
 *  Constructs a bounding box for given point(cloud)s which may be expanded to include further points.
 *
 *  \tparam CoordType the type used to specify point coordinates
 *  \tparam dim the dimension of the embedding space
 */
template<class CoordType, int dim>
class BoundingBox
: public Box<CoordType, dim>
{

    public:

        /** \brief Create box from two given points
         */
        BoundingBox(const CoordType& lower, const CoordType& upper):
            Box<CoordType,dim>(lower, upper)
        {}

        /** \brief Create "box" from one given point 
         */
        BoundingBox(const CoordType& point):
            Box<CoordType,dim>(point, point)
        {}

        /** \brief Create bounding box of a set of points
         */
        BoundingBox(const std::vector<const CoordType>& points)
        {
            /* if point set is empty throw an exception otherwise set lower/upper to the first point in the container */
            if (points.size() == 0)
                DUNE_THROW(Dune::RangeError,"Point set is empty in Constructor BoundingBox::BoundingBox(const std::vector<CoordType>& points)");
            else
            {
                this->upper_ = points[0];
                this->lower_ = points[0];
            }

            /* lower = (min(p) p_1, min(p) p_2, ...); upper = (max(p) p_1, ... */
            for (size_t i=1; i<points.size(); i++)
            {
                for (int j=0; j<dim; j++)
                {
                    if (points[i][j] < this->lower_[j])
                        this->lower_[j] = points[i][j];
                    if (points[i][j] > this->upper_[j])
                        this->upper_[j] = points[i][j];
                }
            }

            /* set the center */
            setCenter();
        }

        /** \brief expands box to include a new point
         *
         *  This method inserts single points into the point cloud and expands the Box correspondingly.
         *
         *  \param point the point to be included
         *  \param resetCenter flag whether the center of the box shall be recalculated after expanding the box
         */
        void expandToInclude(const CoordType& point, bool resetCenter=true)
        {
            for (int j=0; j<dim; j++)
            {
                if (point[j] < this->lower_[j])
                    this->lower_[j] = point[j];
                if (point[j] > this->upper_[j])
                    this->upper_[j] = point[j];
            }

            if (resetCenter)
                setCenter();
        }

        /** \brief calculates and sets the coordinates of the center of the box
         */
        void setCenter()
        {
            for (int i = 0; i < dim; ++i)
                this->center_[i] = 0.5*(this->upper_[i]+this->lower_[i]);

        }

        /* more or less reimplementation of Box::contains, therefore commented out */
        //    /** Checks whether a point is inside of the box
        //     */
        //    bool inside(const CoordType& point) {
        //        for (int i=0; i<dim; i++) {
        //            if (point[i] > this.upper_[i])
        //                return false;
        //            if (point[i] < this.lower_[i])
        //                return false;
        //        }
        //        return true;
        //    }

        /** \brief checks whether a point is inside an epsilon neighborhood of the bounding box
         *
         *  \param point the point to be checked
         *  \param epsilon the extension of the box
         */
        bool insideNeighborhood(const CoordType& point, const double epsilon)
        {
            for (int i=0; i<dim; i++)
            {
                if (point[i] > this->upper_[i] + epsilon)
                    return false;
                if (point[i] < this->lower_[i] - epsilon)
                    return false;
            }
            return true;
        }

};

template<class CoordType, int dim>
std::ostream& operator<< (std::ostream& s, const Box<CoordType, dim>& box)
{
    s << "[ (" << box.lower()[0];
    for (int i=1; i<dim; ++i)
        s << ", " << box.lower()[i];
    s << ") ,(" << box.upper()[0];
    for (int i=1; i<dim; ++i) 
        s << ", " << box.upper()[i]; 
    s << ")]" << std::endl;
    return s;
}

#endif
