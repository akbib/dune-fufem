#ifndef NEWPFEASSEMBLER_HH
#define NEWPFEASSEMBLER_HH

#include <array>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/matrix.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>

#include <dune/fufem/mechanics/elasticitytensor.hh>
#include <dune/fufem/assemblers/localoperatorassembler.hh>
#include <dune/fufem/staticmatrixtools.hh>
#include <dune/fufem/symmetrictensor.hh>


//! \todo Please doc me !
template <class GridType, class TrialLocalFE, class AnsatzLocalFE, class FunctionType, class AlloyType>
class NewPFEAssembler : public LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE, Dune::FieldMatrix<double,GridType::dimension,GridType::dimension> >
{
    private:
        static const int dim = GridType::dimension;

        const FunctionType& phaseField_;
        const AlloyType& alloy_;
        const int quadOrder_;

    public:
        typedef typename Dune::FieldMatrix<double,GridType::dimension,GridType::dimension> T;

        typedef typename LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE, T >::Element Element;
        typedef typename LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE, T >::BoolMatrix BoolMatrix;
        typedef typename LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE, T >::LocalMatrix LocalMatrix;
//        typedef typename LocalOperatorAssembler < GridType ,T >::BoolMatrix BoolMatrix;
//        typedef typename Dune::Matrix<T> LocalMatrix;

        NewPFEAssembler(const FunctionType& phaseField, const AlloyType& alloy, const int quadOrder=2):
            phaseField_(phaseField),
            alloy_(alloy),
            quadOrder_(quadOrder)
        {}

        void indices(const Element& element, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            isNonZero = true;
            return;
        }

        void assemble(const Element& element, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            typedef typename Dune::template FieldVector<double,dim> FVdim;
            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::JacobianType JacobianType;
            typedef typename Element::Geometry Geometry;
            typedef typename Geometry::JacobianInverseTransposed JacobianInverseTransposed;

            int rows = localMatrix.N();

            localMatrix = 0.0;

            // get quadrature rule
//            const Dune::template QuadratureRule<double, dim>& quad = Dune::template QuadratureRules<double, dim>::rule(element.type(), quadOrder_);
            const Dune::template QuadratureRule<double, dim>& quad = QuadratureRuleCache<double, dim>::rule(element.type(), quadOrder_, IsRefinedLocalFiniteElement<TrialLocalFE>::value(tFE) );

            // store gradients of shape functions and base functions
            std::vector<JacobianType> referenceGradients(tFE.localBasis().size());
            std::vector<FVdim> gradients(tFE.localBasis().size());

            // loop over quadrature points
            for (size_t pt=0; pt < quad.size(); ++pt)
            {
                // get quadrature point
                const FVdim& quadPos = quad[pt].position();

                // get transposed inverse of Jacobian of transformation
                const Geometry geometry = element.geometry();
                const JacobianInverseTransposed& invJacobian = geometry.jacobianInverseTransposed(quadPos);

                // get integration factor
                const double integrationElement = geometry.integrationElement(quadPos);

                // Value of the phase field at this quadrature point
                typename FunctionType::RangeType phaseValue;
                phaseField_.evaluateLocal(element, quadPos, phaseValue);

                // ncompspec !!
                //Dune::FieldVector<double,2> phaseFieldVector((1.0+phaseValue)*0.5);   /* this line for order parameter u \in [-1,1] */
                Dune::FieldVector<double,2> phaseFieldVector(phaseValue[0]);            /* this line for order parameter u \in [0,1] */
                phaseFieldVector[1] = 1.0-phaseFieldVector[0];

                // Get the interpolated Hooke tensor
                ElasticityTensor<dim> hookeTensor;
                alloy_.getElasticityTensor(phaseFieldVector, hookeTensor);

                // Get the interpolated intrinsic strain
                SymmetricTensor<dim> intrinsicStrain;
                alloy_.getEigenstrain(phaseFieldVector, intrinsicStrain);

                // get gradients of shape functions
                tFE.localBasis().evaluateJacobian(quadPos, referenceGradients);

                // compute gradients of base functions
                for (size_t i=0; i<gradients.size(); ++i)
                {
                    // transform gradients
                    gradients[i] = 0.0;
                    invJacobian.umv(referenceGradients[i][0], gradients[i]);
                }

                // /////////////////////////////////////////////
                //   Compute strain for all shape functions
                // /////////////////////////////////////////////
                std::vector<std::array<SymmetricTensor<dim>,dim> > strain(rows);

                for (int i=0; i<rows; i++)
                {
                    for (int k=0; k<dim; k++)
                    {
                        // The deformation gradient of the shape function
                        Dune::FieldMatrix<double, dim, dim> deformationGradient(0);
                        deformationGradient[k] = gradients[i];

                        /* Computes the linear strain tensor from the deformation gradient*/
                        computeStrain(deformationGradient,strain[i][k]);

                    }

                }

                // /////////////////////////////////////////////////
                //   Assemble matrix
                // /////////////////////////////////////////////////
                double z = quad[pt].weight() * integrationElement;
                for (int row=0; row<rows; ++row)
                {
                    for (int rcomp=0; rcomp<dim; rcomp++)
                    {
                        // Compute stress
                        SymmetricTensor<dim> stress;
                        hookeTensor.mv(strain[row][rcomp],stress);

                        for (int col=0; col<=row; col++) {
                            for (int ccomp=0; ccomp<dim; ccomp++)
                            {
                                double zij = stress*strain[col][ccomp] * z;
                                localMatrix[row][col][rcomp][ccomp] += zij;
                                if (col!=row)
                                    localMatrix[col][row][ccomp][rcomp] += zij;
                            }
                        }
                    }
                }

            }

            return;
        }

        void computeStrain(const Dune::FieldMatrix<double, dim, dim>& grad, SymmetricTensor<dim>& strain) const
        {
            for (int i=0; i<dim ; ++i)
            {
                strain(i,i) = grad[i][i];
                for (int j=i+1; j<dim; ++j)
                    strain(i,j) = 0.5*(grad[i][j] + grad[j][i]);
            }

        }
    };


#endif

