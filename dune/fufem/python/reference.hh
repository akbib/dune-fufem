// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_PYTHON_REFERENCE_HH
#define DUNE_FUFEM_PYTHON_REFERENCE_HH

// Only introduce the dune-python interface if python
// was found and enabled.
#if HAVE_PYTHON || DOXYGEN

#include <Python.h>

#include <string>
#include <type_traits>

#include <dune/common/exceptions.hh>
#include <dune/common/typetraits.hh>

#include <dune/fufem/formatstring.hh>


namespace Python
{



// forward declaration needed by Python::Reference
class Reference;
template<class T> struct Conversion;
template<class T> Reference makeObject(const T& t);
template<typename... Ts> Reference makeTuple(const Ts&...);
void handlePythonError(const std::string&, const std::string&);



/**
 * \brief Wrapper class for python objects
 *
 * This class wraps all python objects accessed or generated
 * by the dune python interface. It hides all manual reference
 * counting required by the python api from the user.
 *
 * Since the internal reference counting is interesting if
 * you want to extend the interface, e.g., by introducing
 * conversions for new types, the details about the internal
 * reference counting are documented where necessary.
 * To avoid confusing the user this is marked
 * 'For implementors:'
 *
 * For implementors:
 *
 * This class wraps a PyObject* in order to automate reference counting.
 * Most methods of the python api return a PyObject* representing a new
 * reference that is already counted internally. If you want to dispose
 * such a reference you have to call Py_DECREF manually.
 *
 * This class takes over ownership of such a new reference represented by
 * PyObject* and calls Py_DECREF automatically on destruction.
 * You can simply use Reference instead of PyObject* in all cases
 * where new references are returned.
 *
 * Furthermore it wraps some global python api methods as member functions.
 *
 * A note about constness:
 * We can in general not control constness from C correctly.
 * Since the python api only uses non-const pointers we need to store
 * a mutable pointer internally. Constness of the methods of this class
 * is in general only a reasonable indicator. As obvious exception
 * the call() method is marked const, while the python object might change.
 * Even the get() method might change the python object if __getattr__ is
 * overloaded in python.
 */
class Reference
{
    public:

        /**
         * \brief Construct empty Reference
         */
        Reference() :
            p_(0)
        {}

        /**
         * \brief Construct Reference from PyObject*
         *
         * Only to be used if you want to extend the interface
         * using the python api.
         *
         * For implementors:
         *
         * \param p The pointer that will be owned by the Reference
         *
         * Reference(p) steals a reference, i.e.,
         * it does always assume that the PyObject* p is a new reference
         * as returned by most api methods. The Reference will take ownership
         * i.e. the reference count will not be incremented but decremented
         * on destruction.
         *
         * Some api methods return borrowed references. In this case
         * use Reference(Imp::inc(p)) to increment the reference count manually before wrapping.
         */
        Reference(PyObject* p) :
            p_(p)
        {}

        /**
         * \brief Copy constructor
         *
         * For implementors:
         *
         * This will increment the count for the
         * stored reference.
         */
        Reference(const Reference& other) :
            p_(other.p_)
        {
            Py_XINCREF(p_);
        }

        /**
         * \brief Destructor
         *
         * For implementors:
         *
         * This will decrement the count for the stored reference.
         */
        virtual ~Reference()
        {
            if (Py_IsInitialized())
                Py_XDECREF(p_);
        }

        /**
         * \brief Assignment
         *
         * For implementors:
         *
         * Decrements count for old reference and increments count
         * for new reference.
         */
        virtual Reference& operator= (const Reference& other)
        {
            Py_XINCREF(other.p_);
            Py_XDECREF(p_);
            p_ = other.p_;
            return *this;
        }

        /**
         * \brief Access to stored reference
         *
         * Return stored refence without changing its count.
         * You can use this with all api methods that do not
         * steal references.
         *
         * \returns A PyObject* representing a borrowed reference
         */
        operator PyObject* () const
        {
            return p_;
        }

        /**
         * \brief Convert to C object
         *
         * This does not change the reference count.
         *
         * \tparam T C type for conversion
         *
         * \param t Reference to store the result
         */
        template<class T>
        void toC(T& t) const
        {
            assertPyObject("toC()");
            Conversion<T>::toC(p_, t);
        }

        /**
         * \brief Convert to C object
         *
         * This does not change the reference count.
         * This method uses the default constructor of T.
         *
         * \tparam T C type for conversion
         */
        template<class T>
        typename std::enable_if< Conversion<T>::useDefaultConstructorConversion, T>::type toC() const
        {
            assertPyObject("toC()");
            T t;
            Conversion<T>::toC(p_, t);
            return t;
        }

        /**
         * \brief Convert to C object
         *
         * This does not change the reference count.
         * This method uses a type dependent  custom construct.
         *
         * \tparam T C type for conversion
         *
         * \param t Reference to store the result
         */
        template<class T>
        typename std::enable_if< Conversion<T>::useCustomConstructorConversion, T>::type toC() const
        {
            assertPyObject("toC()");
            return Conversion<T>::toC(p_);
        }

        /**
         * \brief Check if object has attribute of given name
         *
         * \param name Name of attribute
         */
        bool hasAttr(const std::string& name) const
        {
            assertPyObject("hasAttr()");
            return PyObject_HasAttrString(p_, name.c_str());
        }

        /**
         * \brief Query attribute of given name
         *
         * \param name Name of attribute
         * \returns The attributes value as Reference
         *
         * For implementors:
         *
         * The count for the returned Reference is incremented.
         */
        Reference get(const std::string& name) const
        {
            assertPyObject("get()");
            // This returns a new reference, i.e., it increments the reference count.
            PyObject* value = PyObject_GetAttrString(p_, name.c_str());
            if (not value)
                handlePythonError("get()", Dune::Fufem::formatString("failed to get attribute '%s'", name.c_str()));
            return value;
        }

        /**
         * \brief Set attribute of given name
         *
         * \param name Name of attribute
         * \param value New value of attribute
         *
         * For implementors:
         *
         * This will not decrease the reference count for
         * the Reference used as argument.
         */
        template<class V>
        void set(const std::string& name, const V& value)
        {
            assertPyObject("set()");
            if (PyObject_SetAttrString(p_, name.c_str(), makeObject(value))==-1)
                handlePythonError("set()", Dune::Fufem::formatString("failed to set attribute '%s'", name.c_str()));
        }

        /**
         * \brief Call this Reference with arguments given as tuple
         * 
         * If the Reference represents a function it's called.
         * If the Reference represents an instance its __call__ method is invoked.
         * If the Reference represents a class a constructor is invoked.
         * In any case the arguments are given as a tuple as obtained by the
         * global method Python::makeTuple().
         *
         * Although the method is const it might change the refered object!
         * This cannot be avoided since the python api does not know
         * about const pointers so we use a mutable pointer internally.
         *
         * \param args Arguments represented as tuple
         * \returns The result of the call
         *
         */
        Reference callWithArgumentTuple(const Reference& args) const DUNE_DEPRECATED_MSG("Replace ref.callWithArgumentTuple(args) by Callable(ref).callWithArgumentTuple(args).")
        {
            return callWithArgumentTupleImp(args);
        }

        /**
         * \brief Call this object without arguments
         *
         * Shortcut for callWithArgumentTuple(makeTuple()).
         */
        Reference operator() () const DUNE_DEPRECATED_MSG("Replace ref() by Callable(ref)().")
        {
            return callWithArgumentTupleImp(makeTuple());
        }

        /**
         * \brief Call this object with one argument
         *
         * Shortcut for callWithArgumentTuple(makeTuple(t0)).
         */
        template<class T0>
        DUNE_DEPRECATED_MSG("Replace ref(t0) by Callable(ref)(t0).")
        Reference operator() (const T0& t0) const
        {
            return callWithArgumentTupleImp(makeTuple(t0));
        }

        /**
         * \brief Call this object with two argument
         *
         * Shortcut for callWithArgumentTuple(makeTuple(t0,t1)).
         */
        template<class T0, class T1>
        DUNE_DEPRECATED_MSG("Replace ref(t0,t1) by Callable(ref)(t0,t1).")
        Reference operator() (const T0& t0, const T1& t1) const
        {
            return callWithArgumentTupleImp(makeTuple(t0, t1));
        }

        /**
         * \brief Call this object with three argument
         *
         * Shortcut for callWithArgumentTuple(makeTuple(t0,t1,t2)).
         */
        template<class T0, class T1, class T2>
        DUNE_DEPRECATED_MSG("Replace ref(t0,t1,t2) by Callable(ref)(t0,t1,t2).")
        Reference operator() (const T0& t0, const T1& t1, const T2& t2) const
        {
            return callWithArgumentTupleImp(makeTuple(t0, t1, t2));
        }

        /**
         * \brief String representation of this object
         */
        std::string str() const
        {
            assertPyObject("str()");
#if PY_MAJOR_VERSION >= 3
            char* s = PyUnicode_AsUTF8(PyObject_Str(p_));
#else
            char* s = PyString_AsString(PyObject_Str(p_));
#endif
            if (not s)
                handlePythonError("str()", "cannot obtain string representation");
            return s;
        }

    protected:

        // This method only exists since the deprecated operator() methods
        // would otherwise have to call the deprecated callWithArgumentTuple()
        // method leading to a warning even if you don't use them.
        // This will be removed if the deprecated method is removed.
        Reference callWithArgumentTupleImp(const Reference& args) const
        {
            assertPyObject("callWithArgumentTuple()");
            PyObject* result = PyObject_CallObject(p_, args);
            if (not result)
                handlePythonError("callWithArgumentTuple()", "failed to call object");
            return result;
        }


        /**
         * \brief Assert that internal PyObject* is not NULL and raise exception otherwise
         *
         * \param origin A string describing the origin of the error
         */
        void assertPyObject(const std::string& origin) const
        {
            if (not p_)
                DUNE_THROW(Dune::Exception,
                    "Python error occured." << std::endl <<
                    "  Origin:  " << origin << std::endl <<
                    "  Error:   Python::Reference points to NULL object");
        }

        // We must store a mutable pointer since all methods of the
        // python api use nonsonst PyObject* pointers.
        mutable PyObject* p_;
};


} // end of namespace Python



#else
    #warning dunepython.hh was included but python was not found or enabled!
#endif // DUNE_FUFEM_PYTHON_REFERENCE_HH


#endif

