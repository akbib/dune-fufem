// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_PYTHON_FUNCTION_HH
#define DUNE_FUFEM_PYTHON_FUNCTION_HH

// Only introduce the dune-python interface if python
// was found and enabled.
#if HAVE_PYTHON || DOXYGEN

#include <Python.h>

#include <string>
#include <sstream>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/function.hh>
#include <dune/common/typetraits.hh>

#include <dune/fufem/functions/virtualfunctiontoboundarysegmentadapter.hh>
#include <dune/fufem/functions/virtualdifferentiablefunction.hh>

#include <dune/fufem/python/reference.hh>
#include <dune/fufem/python/callable.hh>
#include <dune/fufem/python/common.hh>
#include <dune/fufem/python/conversion.hh>



/**
 * \brief Function using embedded python for evaluation.
 *
 * \tparam DT Domain type
 * \tparam RT Range type
 */
template<class DT, class RT>
class PythonFunction :
    public Dune::VirtualFunction<DT, RT>
{
    public:

        /**
         * \brief Create PythonFunction wrapping a callable python object
         */
        PythonFunction(const Python::Callable callable)
        {
            if (not callable)
                DUNE_THROW(Dune::Exception, "Trying to construct PythonFunction from NULL");
            callable_ = callable;
        }

        /**
         * \brief Create PythonFunction by name of a python function
         *
         * This creates a PythonFunction that
         * wraps an already existing function
         * with the given name.
         */
        PythonFunction(const std::string& name) DUNE_DEPRECATED_MSG("Use PythonFunction(Python::main().get(name) instead")
        {
            // get reference for python function
            Python::Callable callable = Python::main().get(name);
            if (not callable)
                DUNE_THROW(Dune::Exception, "Trying to construct PythonFunction from NULL");
            callable_ = callable;
        }

        /**
         * \brief Create PythonFunction from string expression.
         *
         * This will first create a python function with the given name
         * that evaluates the given expression and than wrap
         * this as PythonFunction.
         *
         * If expression contains no newline it is directly
         * evaluated and returned. If it contains a newline
         * it is used as function body and expected to call
         * return itself.
         */
        PythonFunction(const std::string& name, const std::string& expression)
        {
            // create a python function
            std::stringstream s;
            std::string::size_type newline = expression.find("\n");
            if (newline != std::string::npos)
                s << "def " << name << "(x):\n" << expression;
            else
                s << "def " << name << "(x):\n    return " << expression;

            // Execute code in __main__ and get reference for python function
            Python::Module main = Python::main();
            main.run(s.str());
            callable_ = main.get(name);
        }

        /**
         * \copydoc VirtualFunction::evaluate
         */
        void evaluate(const DT& x, RT& y) const
        {
            callable_(x).toC(y);
        }

        /**
         * \brief Evaluate function
         */
        RT operator()(const DT& x) const
        {
            return callable_(x).template toC<RT>();
        }

    protected:

        Python::Callable callable_;
};


/**
 * \brief Function using embedded python for evaluation of values and derivatives.
 *
 * \tparam DT Domain type
 * \tparam RT Range type
 */
template<class DT, class RT>
class DifferentiablePythonFunction :
    public VirtualDifferentiableFunction<DT, RT>
{
        typedef VirtualDifferentiableFunction<DT, RT> Base;
    public:
        typedef typename Base::DomainType DomainType;
        typedef typename Base::RangeType RangeType;
        typedef typename Base::DerivativeType DerivativeType;

        /**
         * \brief Create DifferentiablePythonFunction wrapping two callable python object
         */
        DifferentiablePythonFunction(const Python::Callable value, const Python::Callable derivative)
        {
            if (not value)
                DUNE_THROW(Dune::Exception, "Trying to construct DifferentiablePythonFunction with NULL as value argument");
            if (not derivative)
                DUNE_THROW(Dune::Exception, "Trying to construct DifferentiablePythonFunction with NULL as derivative argument");
            value_ = value;
            derivative_ = derivative;
        }

        /**
         * \brief Create DifferentiablePythonFunction from sequence of callables
         */
        DifferentiablePythonFunction(const Python::Reference fdf)
        {
            if (not Python::isSequence(fdf))
                DUNE_THROW(Dune::Exception, "Trying to construct DifferentiablePythonFunction from nonsequence python object");
            int size = Python::size(fdf);
            if (size<2)
                DUNE_THROW(Dune::Exception, "Trying to construct DifferentiablePythonFunction from sequence with size<2");
            Python::Callable value(Python::getItem(fdf, 0));
            Python::Callable derivative(Python::getItem(fdf, 1));
            if (not value)
                DUNE_THROW(Dune::Exception, "Trying to construct DifferentiablePythonFunction with NULL as value argument");
            if (not derivative)
                DUNE_THROW(Dune::Exception, "Trying to construct DifferentiablePythonFunction with NULL as derivative argument");
            value_ = value;
            derivative_ = derivative;
        }

        /**
         * \copydoc VirtualFunction::evaluate
         */
        void evaluate(const DomainType& x, RangeType& y) const
        {
            value_(x).toC(y);
        }

        /**
         * \copydoc VirtualDifferentiableFunction::evaluateDerivative
         */
        void evaluateDerivative(const DomainType& x, DerivativeType& y) const
        {
            derivative_(x).toC(y);
        }

        /**
         * \brief Evaluate function
         */
        RT operator()(const DT& x) const
        {
            return callable_(x).template toC<RT>();
        }

    protected:

        Python::Callable value_;
        Python::Callable derivative_;
};


namespace Python
{

// conversion of callable to PythonFunction*
template<class DT, class RT>
struct Conversion<Dune::VirtualFunction<DT, RT>*>
{
    enum {useDefaultConstructorConversion=true};
    static void toC(PyObject* pyF, Dune::VirtualFunction<DT, RT>*& f)
    {
        // We don't want to steal a reference - hence we must call inc().
        f = new PythonFunction<DT, RT>(Reference(Imp::inc(pyF)));
    }
};

// conversion of callable to PythonFunction*
template<class DT, class RT>
struct Conversion<PythonFunction<DT, RT>*>
{
    enum {useDefaultConstructorConversion=true};
    static void toC(PyObject* pyF, PythonFunction<DT, RT>*& f)
    {
        // We don't want to steal a reference - hence we must call inc().
        f = new PythonFunction<DT, RT>(Reference(Imp::inc(pyF)));
    }
};

// conversion of callable to BoundarySegment*
template<int dim, int dimworld>
struct Conversion<Dune::BoundarySegment<dim,dimworld>*>
{
    enum {useDefaultConstructorConversion=true};
    static void toC(PyObject* pyF, Dune::BoundarySegment<dim,dimworld>*& segmentPointer)
    {
        typedef Dune::FieldVector<double,dim-1> DomainType;
        typedef Dune::FieldVector<double,dimworld> RangeType;

        // Wrap raw PyObject without stealing a reference
        Reference f = Reference(Imp::inc(pyF));

        typedef VirtualFunctionToBoundarySegmentAdapter<dim, dimworld> Adapter;
        segmentPointer = new Adapter(typename Adapter::FunctionSharedPtr(new PythonFunction<DomainType, RangeType>(f)));
    }
};

// conversion of sequence of callables to VirtualDifferentiableFunction*
template<class DT, class RT>
struct Conversion<VirtualDifferentiableFunction<DT, RT>*>
{
    enum {useDefaultConstructorConversion=true};
    static void toC(PyObject* pyFDF, VirtualDifferentiableFunction<DT, RT>*& f)
    {
        // We don't want to steal a reference - hence we must call inc().
        f = new DifferentiablePythonFunction<DT, RT>(Reference(Imp::inc(pyFDF)));
    }
};

// conversion of sequence of callables to DifferentiablePythonFunction*
template<class DT, class RT>
struct Conversion<DifferentiablePythonFunction<DT, RT>*>
{
    enum {useDefaultConstructorConversion=true};
    static void toC(PyObject* pyFDF, DifferentiablePythonFunction<DT, RT>*& f)
    {
        // We don't want to steal a reference - hence we must call inc().
        f = new DifferentiablePythonFunction<DT, RT>(Reference(Imp::inc(pyFDF)));
    }
};

} // end of namespace Python


#else
    #warning dunepython.hh was included but python was not found or enabled!
#endif // DUNE_FUFEM_PYTHON_FUNCTION_HH


#endif

