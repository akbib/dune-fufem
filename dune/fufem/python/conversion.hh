// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_PYTHON_CONVERSION_HH
#define DUNE_FUFEM_PYTHON_CONVERSION_HH

// Only introduce the dune-python interface if python
// was found and enabled.
#if HAVE_PYTHON || DOXYGEN

#include <Python.h>

#include <memory>
#include <string>
#include <sstream>
#include <vector>
#include <map>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/typetraits.hh>
#include <dune/grid/common/gridfactory.hh>

#include <dune/fufem/functions/virtualfunctiontoboundarysegmentadapter.hh>
#include <dune/fufem/formatstring.hh>

#include <dune/fufem/python/reference.hh>
#include <dune/fufem/python/common.hh>


namespace Python
{

// *****************************************************************************
// specializations of Conversion that do the PyObject* <-> C++-type conversion
// *****************************************************************************

// conversion of double
template<>
struct Conversion<double>
{
    enum {useDefaultConstructorConversion=true};
    static void toC(PyObject* x, double& y) { y = PyFloat_AsDouble(x); }
    static PyObject* toPy(const double& x) { return PyFloat_FromDouble(x); }
};

// conversion of int
template<>
struct Conversion<int>
{
    enum {useDefaultConstructorConversion=true};

#if PY_MAJOR_VERSION >= 3
    static void toC(PyObject* x, int& y) { y = PyLong_AsLong(x); }
    static PyObject* toPy(const int& x) { return PyLong_FromLong(x); }
#else
    static void toC(PyObject* x, int& y) { y = PyInt_AsLong(x); }
    static PyObject* toPy(const int& x) { return PyInt_FromLong(x); }
#endif
};

// conversion of long
template<>
struct Conversion<long>
{
    enum {useDefaultConstructorConversion=true};

    static void toC(PyObject* x, long& y) { y = PyLong_AsLong(x); }
    static PyObject* toPy(const long& x) { return PyLong_FromLong(x); }
};

// conversion of bool
template<>
struct Conversion<bool>
{
    enum {useDefaultConstructorConversion=true};
    static void toC(PyObject* x, bool& y) {
        int i = PyObject_IsTrue(x);
        if (i==-1)
            handlePythonError("Conversion<bool>::toC()", "cannot interpret python object as bool");
        y = i;
    }
    static PyObject* toPy(const bool& x) { return PyBool_FromLong(x); }
};

// conversion of unsigned int
template<>
struct Conversion<unsigned int>
{
    enum {useDefaultConstructorConversion=true};
#if PY_MAJOR_VERSION >= 3
    static void toC(PyObject* x, unsigned int& y) { y = PyLong_AsUnsignedLongMask(x); }
    static PyObject* toPy(const unsigned int& x) { return PyLong_FromLong(x); }
#else
    static void toC(PyObject* x, unsigned int& y) { y = PyInt_AsUnsignedLongMask(x); }
    static PyObject* toPy(const unsigned int& x) { return PyInt_FromLong(x); }
#endif
};

// conversion of string
template<>
struct Conversion<std::string>
{
    enum {useDefaultConstructorConversion=true};

#if PY_MAJOR_VERSION >= 3
    static void toC(PyObject* x, std::string& y) { y = PyUnicode_AsUTF8(x); }
    static PyObject* toPy(const std::string& x) { return Imp::inc(PyUnicode_FromString(x.c_str())); }
#else
    static void toC(PyObject* x, std::string& y) { y = PyString_AsString(x); }
    static PyObject* toPy(const std::string& x) { return PyString_FromString(x.c_str()); }
#endif
};

// conversion of raw char[]
template<int i>
struct Conversion<char[i]>
{
    static void toC(PyObject* x, char y[i])
    {
        DUNE_THROW(Dune::Exception, "Conversion to C type char[] not implemented");
    }

#if PY_MAJOR_VERSION >= 3
    static PyObject* toPy(const char x[i]) { return Imp::inc(PyUnicode_FromString(x)); }
#else
    static PyObject* toPy(const char x[i]) { return PyString_FromString(x); }
#endif
};

// conversion to shared_ptr
template<class P>
struct Conversion<std::shared_ptr<P> >
{
    enum {useDefaultConstructorConversion=true};
    static void toC(PyObject* o, std::shared_ptr<P>& p)
    {
        P* rawP;
        Conversion<P*>::toC(o, rawP);
        p = std::shared_ptr<P>(rawP);
    }
    static PyObject* toPy(const std::shared_ptr<P>& x)
    {
        DUNE_THROW(Dune::Exception, "Conversion from C type std::shared_ptr<" << typeid(P).name() << "> not implemented");
        return 0;
    }
};

// conversion of vector
template<class T>
struct Conversion<std::vector<T> >
{
    enum {useDefaultConstructorConversion=true};
    static void toC(PyObject* list, std::vector<T>& v)
    {
        if (not PySequence_Check(list))
            DUNE_THROW(Dune::Exception, "cannot convert a non-sequence to a std::vector");
        int size = PySequence_Size(list);
        v.resize(size);
        for(int i=0; i<size; ++i)
            Reference(PySequence_GetItem(list, i)).toC(v[i]);
        // The above is OK since PySequence_GetItem returns a new reference.
        // With PyList_GetItem we would need one of the following since it
        // returns a borrowed reference:
        // Conversion<T>::toC(PyList_GetItem(list, i), v[i]);
        // Reference(Imp::inc(PyList_GetItem(list, i))).toC(v[i]);
    }
    static PyObject* toPy(const std::vector<T>& v)
    {
        PyObject* list = PyList_New(v.size());
        for(size_t i=0; i<v.size(); ++i)
            PyList_SetItem(list, i, Imp::inc(makeObject(v[i])));
        // We have to use inc() since PyList_SetItem STEALS a reference!
        return list;
    }
};

// conversion to dict to map
template<class K, class V, class C, class A>
struct Conversion<std::map<K, V, C, A> >
{
    enum {useDefaultConstructorConversion=true};
    static void toC(PyObject* dict, std::map<K, V, C, A>& map)
    {
        if (not PyDict_Check(dict))
            DUNE_THROW(Dune::Exception, "cannot convert a non-dictionary to a std::map");
        PyObject* key;
        PyObject* value;
        Py_ssize_t pos = 0;
        while (PyDict_Next(dict, &pos, &key, &value))
        {
            K cKey;
            V cValue;
            Reference(Imp::inc(key)).toC(cKey);
            Reference(Imp::inc(value)).toC(cValue);
            map[cKey] = cValue;
        }
    }

    static PyObject* toPy(const std::map<K, V, C, A>& map)
    {
        DUNE_THROW(Dune::Exception, "Conversion from C type std::map<K,V,C,A> not implemented");
        return 0;
    }
};

// conversion of FieldVector
template<class T, int n>
struct Conversion<Dune::FieldVector<T,n> >
{
    enum {useDefaultConstructorConversion=true};
    static void toC(PyObject* list, Dune::FieldVector<T,n>& v)
    {
        if (not PySequence_Check(list))
        {
            if (n!=1)
                DUNE_THROW(Dune::Exception, "cannot convert a non-sequence to a Dune::FieldVector<T,n> with n>1");

            Reference(Imp::inc(list)).toC(v[0]);
        }
        else
        {
            if (PySequence_Size(list)!=n)
                DUNE_THROW(Dune::Exception, "cannot convert a sequence to a Dune::FieldVector with a different size");

            for(size_t i=0; i<n; ++i)
                Reference(PySequence_GetItem(list, i)).toC(v[i]);
            // The above is OK since PySequence_GetItem returns a new reference.
            // With PyTuple_GetItem we would need one of the following since it
            // returns a borrowed reference:
            // Conversion<T>::toC(PyTuple_GetItem(list, i), v[i]);
            // Reference(Imp::inc(PyTuple_GetItem(list, i))).toC(v[i]);
        }
    }

    static PyObject* toPy(const Dune::FieldVector<T,n>& v)
    {
        PyObject* tuple = PyTuple_New(n);
        for(int i=0; i<n; ++i)
            PyTuple_SetItem(tuple, i, Imp::inc(makeObject(v[i])));
        // We have to use inc() since PyList_SetItem STEALS a reference!
        return tuple;
    }
};

// conversion of FieldMatrix
template<class T, int n, int m>
struct Conversion<Dune::FieldMatrix<T,n,m> >
{
    enum {useDefaultConstructorConversion=true};
    static void toC(PyObject* list, Dune::FieldMatrix<T,n,m>& v)
    {
        if (not PySequence_Check(list))
        {
            if (n!=1)
                DUNE_THROW(Dune::Exception, "cannot convert a non-sequence to a Dune::FieldMatrix<T,n,m> with n>1");

            Reference(Imp::inc(list)).toC(v[0]);
        }
        else
        {
            if (PySequence_Size(list)!=n)
                DUNE_THROW(Dune::Exception, "cannot convert a sequence to a Dune::FieldMatrix<T,n,m> with a different size");

            for(size_t i=0; i<n; ++i)
                Reference(PySequence_GetItem(list, i)).toC(v[i]);
            // The above is OK since PySequence_GetItem returns a new reference.
            // With PyTuple_GetItem we would need one of the following since it
            // returns a borrowed reference:
            // Conversion<T>::toC(PyTuple_GetItem(list, i), v[i]);
            // Reference(Imp::inc(PyTuple_GetItem(list, i))).toC(v[i]);
        }
    }

    static PyObject* toPy(const Dune::FieldMatrix<T,n,m>& v)
    {
        PyObject* tuple = PyTuple_New(n);
        for(int i=0; i<n; ++i)
            PyTuple_SetItem(tuple, i, Imp::inc(makeObject(v[i])));
        // We have to use inc() since PyList_SetItem STEALS a reference!
        return tuple;
    }
};

// conversion to dict to ParameterTree
template<>
struct Conversion<Dune::ParameterTree>
{
    enum {useDefaultConstructorConversion=true};
    static void toC(PyObject* dict, Dune::ParameterTree& param)
    {
        if (not PyDict_Check(dict))
            DUNE_THROW(Dune::Exception, "cannot convert a non-dictionary to a Dune::ParameterTree");
        PyObject* key;
        PyObject* value;
        Py_ssize_t pos = 0;
        while (PyDict_Next(dict, &pos, &key, &value))
        {
            std::string cKey = Reference(Imp::inc(key)).str();

            if (PyDict_Check(value))
                Reference(Imp::inc(value)).toC(param.sub(cKey));
            else
                param[cKey] = Reference(Imp::inc(value)).str();
        }
    }

    static PyObject* toPy(const Dune::ParameterTree& map)
    {
        DUNE_THROW(Dune::Exception, "Conversion from C type Dune::ParameterTree not implemented");
        return 0;
    }
};

/**
 * \brief Conversion of python object to GridFactory
 *
 * The python object is required to have the members vertices, elements, segments
 * each of them being an iterable object. These could be simple sequence types
 * like lists. However, one can also use Python generators to create everything
 * on the fly.
 *
 * A vertex is represented by a sequence containing its coordinates.
 * An element is represented by a sequence containing the indices of its vertices.
 * A segment is represented by a sequence containing the indices of its vertices.
 * If a segment is also callable, it is used as parametrization for this segment.
 *
 * The conversion fills the GridFactory with the data provides by the python
 * object constructing wrappers for the given boundary segments.
 */
template<class GridType>
struct Conversion<Dune::GridFactory<GridType> >
{
    enum {useDefaultConstructorConversion=true};

    static void toC(PyObject* pyGridRaw, Dune::GridFactory<GridType>& gridFactory)
    {
        const int dim = GridType::dimension;
        const int dimworld = GridType::dimensionworld;

        typedef typename Dune::FieldVector<typename GridType::ctype, dim> Vertex;
        typedef typename std::vector<unsigned int> Element;
        typedef typename std::vector<unsigned int> Face;
        typedef Dune::BoundarySegment<dim,dimworld> Segment;
        typedef std::shared_ptr<Segment> SegmentPointer;

        Reference grid(Imp::inc(pyGridRaw));

        Reference vertexIt = iter(grid.get("vertices"));
        for(Reference vertex = next(vertexIt); vertex; vertex = next(vertexIt))
            gridFactory.insertVertex(vertex.toC<Vertex>());

        Reference elementIt = iter(grid.get("elements"));
        for(Reference element = next(elementIt); element; element = next(elementIt))
        {
            auto type = Dune::geometryTypeFromVertexCount(dim, size(element));
            gridFactory.insertElement(type, element.toC<Element>());
        }

        if (grid.hasAttr("segments"))
        {
            Reference segmentIt = iter(grid.get("segments"));
            for(Reference segment = next(segmentIt); segment; segment = next(segmentIt))
            {
                if (Python::isCallable(segment))
                    gridFactory.insertBoundarySegment(segment.toC<Face>(), segment.toC<SegmentPointer>());
                else
                    gridFactory.insertBoundarySegment(segment.toC<Face>());
            }
        }
    }
};


} // end of namespace Python



#else
    #warning dunepython.hh was included but python was not found or enabled!
#endif // DUNE_FUFEM_PYTHON_CONVERSION_HH


#endif

