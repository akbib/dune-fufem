// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_PYTHON_COMMON_HH
#define DUNE_FUFEM_PYTHON_COMMON_HH

// Only introduce the dune-python interface if python
// was found and enabled.
#if HAVE_PYTHON || DOXYGEN

#include <Python.h>

#include <string>
#include <sstream>
#include <type_traits>

#include <dune/common/exceptions.hh>
#include <dune/common/std/apply.hh>

#include <dune/functions/common/signature.hh>
#include <dune/functions/common/differentiablefunctionfromcallables.hh>

#include <dune/fufem/python/reference.hh>
#include <dune/fufem/python/module.hh>
#include <dune/fufem/python/callable.hh>


namespace Python
{

// Forward declarations
bool isModule(const Reference&);





// *****************************************************************************
// Base class of Conversion that do the PyObject* <-> C++-type conversion
// *****************************************************************************

/**
 * \brief Convertion of C type T from and to PyObject*
 *
 * Only to be used if you want to extend the interface
 * using the python api. As user you should use
 *
 * \code
 * Python::makeObject();
 * Python::Reference::toC();
 * \endcode
 *
 * to convert a C object to a python object or vice versa.
 *
 * For implementors:
 *
 * \tparam T C type to convert
 *
 * Specialize this to implement conversion for a type T.
 */
template<class T>
struct Conversion
{
    static void toC(PyObject* x, T& y)
    {
        DUNE_THROW(Dune::Exception, "Conversion to C type " << typeid(T).name() << " not implemented");
    }

    static PyObject* toPy(const T& x)
    {
        DUNE_THROW(Dune::Exception, "Conversion from C type " << typeid(T).name() << " not implemented");
        return 0;
    }
};



// *****************************************************************************
// The following are helper functions for implementors and should not be
// used by user code in order to avoid messing up reference counts.
// *****************************************************************************
namespace Imp
{
    PyObject* inc(PyObject* p)
    {
        Py_XINCREF(p);
        return p;
    }

    Reference& inc(Reference& p)
    {
        Py_XINCREF(p);
        return p;
    }

    // Calling makeObject() with a type derived from Reference should
    // just return a Reference to the same object.
    template<class T>
    typename std::enable_if<std::is_base_of<Reference,T>::value, Reference>::type
    makeObject(const T& t)
    {
        return t;
    }

    // Calling makeObject() with any type not derived from Reference should
    // convert this object to a Reference.
    template<class T>
    typename std::enable_if<not(std::is_base_of<Reference,T>::value), Reference>::type
    makeObject(const T& t)
    {
        return Conversion<T>::toPy(t);
    }


} // end of namespace Imp


// *****************************************************************************
// The following methods simplify the usage of the python api from C++.
// *****************************************************************************

/**
 * \brief Start embedded python interpreter.
 *
 * This must be called before any other python action.
 */
void start()
{
    // initialize without registering signal handlers
    // otherwise you cannot stop the program with ctrl-c
    Py_InitializeEx(0);
}

/**
 * \brief Stop embedded python interpreter.
 *
 * This must be called after any other python action
 * before the program terminates.
 */
void stop()
{
    Py_Finalize();
}

/**
 * \brief Import python module given by name.
 *
 * The first time you call the import() method the module
 * is actually imported and the contained initialization
 * code is executed. Later calls will only return
 * a reference to the already imported module
 * without executing the initialization code again.
 *
 * \param moduleName Name of the module to import
 * \returns Python module of given name
 */
Module import(const std::string& moduleName)
{
    Module pModule = PyImport_ImportModule(moduleName.c_str());
    if (not pModule)
        DUNE_THROW(Dune::Exception, "failed to retrieve python module " << moduleName);
    return pModule;
}

/**
 * \brief Create python module given by name.
 *
 * Create a new python module with given name.
 * If a module with the given name was already imported,
 * then this module is returned.
 *
 * Otherwise a new empty module is created and returned,
 * even if a module with the given name is in the search
 * path.
 *
 * \param moduleName Name of the module to import
 * \returns Python module of given name
 */
Module createModule(const std::string& moduleName)
{
    // PyImport_AddModule returnes a borrowed reference so we have to call inc.
    Module pModule = Imp::inc(PyImport_AddModule(moduleName.c_str()));
    if (not pModule)
        DUNE_THROW(Dune::Exception, "failed to create python module " << moduleName);
#if PY_MAJOR_VERSION >= 3
    Reference pBuiltin = Python::import("builtins");
#else
    Reference pBuiltin = Python::import("__builtin__");
#endif
    if (pBuiltin)
        pModule.set("__builtins__", pBuiltin);
    return pModule;
}

/**
 * \brief Obtain the main module
 * 
 * This is a shortcut for import("__main__")
 *
 * \returns Python main module
 */
Module main()
{
    return import("__main__");
}

/**
 * \brief Run python code given as string
 *
 * This executes the python code in the given string
 * in the __main__ module.
 *
 * Although you can also execute several lines using
 * the run() method by separated them with '\n',
 * it will be much more comfortable using the runStream()
 * method.
 *
 * \param code A string containing python code
 */
void run(const std::string& code)
{
    PyRun_SimpleString(code.c_str());
}

/**
 * \brief Run python code given as string in specified module
 *
 * This is the same as run(code) except that
 * the code is executed in the context of the
 * given module. Hence all names are resolved
 * or created in this module.
 *
 * \param code A string containing python code
 * \param module A python object referring to a module
 */
void run(const std::string& code, Reference& module)
{
    if (not isModule(module))
        DUNE_THROW(Dune::Exception, "tried to use non-module where a module is expected");
    Python::Reference dict = module.get("__dict__");
    PyRun_String(code.c_str(), Py_file_input, dict, dict);
}

/**
 * \brief Obtain a stream to feed the code with multiple lines of python code
 *
 * This returns a AutoRunCodeStream object that can be feeded with
 * multiple lines of python code. Once the stream is destroyed
 * the code is executed. This function allows to execute multiline
 * python code in a more comfortable way:
 *
 * \code
 * Python::runStream()
 *  << std::endl << "def add(x,y):"
 *  << std::endl << "  return x+y";
 * \endcode
 *
 * This could not be done by multiple Python::run()
 * statements since you have to do all function/class
 * definitions at once.
 *
 * This executes the python code in the given string
 * in the __main__ namespace.
 *
 * \returns A stream that will execute all feeded code on destruction
 */
Module::AutoRunCodeStream runStream()
{
    return Module::AutoRunCodeStream();
}

/**
 * \brief Obtain a stream to feed the code with multiple lines of python code for execution in specified module
 *
 * This is the same as runStream() except that
 * the code is executed in the context of the
 * given module. Hence all names are resolved
 * or created in this module.
 *
 * \param module A python object referring to a module
 * \returns A stream that will execute all feeded code on destruction
 */
Module::AutoRunCodeStream runStream(Module module)
{
    return module.runStream();
}

/**
 * \brief Run python code in file given by name
 *
 * This executes the python code in the given file
 * in the __main__ namespace.
 *
 * \param fileName A string containing the file name
 */
void runFile(const std::string& fileName)
{
    // the last argument specifies that the file should be closed
    // after executing the code
    PyRun_SimpleFileEx(fopen(fileName.c_str(), "r"),fileName.c_str(), 1);
}

/**
 * \brief Run python code in file given by name in specified module
 *
 * This is the same as runFile(fileName) except that
 * the code is executed in the context of the
 * given module. Hence all names are resolved
 * or created in this module.
 *
 * \param fileName A string containing the file name
 * \param module A python object referring to a module
 */
void runFile(const std::string& fileName, Module module)
{
    module.runFile(fileName);
}

/**
 * \brief Evaluate python expression given as string
 *
 * This evaluates a python expressions in the given string
 * in the __main__ namespace.
 *
 * \param expression A string containing a python expression 
 * \returns Result of expression evaluation
 */
Reference evaluate(const std::string& expression)
{
    Module main = Python::import("__main__");
    return main.evaluate(expression);
}

Reference evaluate(const std::string& expression, Module module)
{
    return module.evaluate(expression);
}

/**
 * \brief Create python object from C++ object
 *
 * \tparam T Type to convert
 *
 * \param x A C++ object from any type.
 * \returns New Reference to python object
 */
template<class T>
Reference makeObject(const T& t)
{
    return Imp::makeObject(t);
}

/**
 * \brief create a Python tuple
 *
 * \tparam Ts types of tuple elements
 * \param  ts tuple elements
 * \returns a new tuple
 */
template<typename... Ts>
Reference makeTuple(const Ts&... ts)
{
    return PyTuple_Pack(sizeof...(Ts), (static_cast<PyObject*>(makeObject(ts)))...);
}

template<typename... Ts>
Reference tuple(const Ts&... ts)
{
    return makeTuple(ts...);
}

/**
 * \brief Get size of a python sequence
 *
 * \param s A python sequence
 * \returns Size of s
 */
int size(const Reference& s)
{
    return PySequence_Size(s);
}

/**
 * \brief Get iterator of iterable object
 *
 * \param seq An iterable python object
 * \returns An iterator for the iterable object
 *
 * For implementors:
 *
 * This increments the reference count of the returned object.
 */
Reference iter(const Reference& seq)
{
    return PyObject_GetIter(seq);
}

/**
 * \brief Get next item from iterator
 *
 * \param iter An iterator object
 * \returns The next object the iterator points to
 *
 * For implementors:
 *
 * This increments the reference count of the returned object.
 */
Reference next(const Reference& iter)
{
    return PyIter_Next(iter);
}

/**
 * \brief Get item of sequence
 *
 * \param seq A python sequence
 * \param i Index of item
 * \returns The i-th item of s
 *
 * For implementors:
 *
 * This increments the reference count of the returned object.
 */
Reference getItem(const Reference& seq, int i)
{
    return PySequence_GetItem(seq, i);
}

/**
 * \brief Get item of dictionary
 *
 * \param dict A python dictionary
 * \param k Key of item
 * \returns The value of the item
 *
 * For implementors:
 *
 * This increments the reference count of the returned object.
 */
Reference getItem(const Reference& dict, const Reference& k)
{
    // We have to use inc() since PyDict_GetItem returns a borrowed reference!
    return Reference(Imp::inc(PyDict_GetItem(dict, k)));
}

/**
 * \brief Set item of sequence
 *
 * \param seq A python sequence
 * \param i Index of item
 * \param v New value of i-th item
 *
 * For implementors:
 *
 * This does not decrement the reference count of the value argument.
 */
template<class V>
void setItem(Reference& seq, int i, const V& v)
{
    PySequence_SetItem(seq, i, makeObject(v));
}

/**
 * \brief Set item of dictionary
 *
 * \param dict A python sequence
 * \param k Key of item
 * \param v New value of the item
 *
 * For implementors:
 *
 * This does not decrement the reference count of the value argument.
 */
template<class K, class V>
void setItem(Reference& dict, const K& k, const V& v)
{
    PyDict_SetItem(dict, makeObject(k), makeObject(v));
}

/**
 * \brief Check if python object is some sequence
 *
 * \param ref A python object
 * \returns true if ref is a sequence, false otherwise
 */
bool isSequence(const Reference& ref)
{
    return PySequence_Check(ref);
}

/**
 * \brief Check if python object is a dictionary
 *
 * \param ref A python object
 * \returns true if ref is a dictionary, false otherwise
 */
bool isDict(const Reference& ref)
{
    return PyDict_Check(ref);
}

/**
 * \brief Check if python object is a module
 *
 * \param ref A python object
 * \returns true if ref is a module, false otherwise
 */
bool isModule(const Reference& ref)
{
    return ref && PyModule_Check(ref);
}

/**
 * \brief Check if python object is callable
 *
 * \param ref A python object
 * \returns true if ref is callable, false otherwise
 */
bool isCallable(const Reference& ref)
{
    return ref && PyCallable_Check(ref);
}

/**
 * \brief Get keys of python dictionary
 *
 * \param dict A python sequence
 * \returns A list containing the keys of the dictionary
 */
Reference keys(const Reference& dict)
{
    return PyDict_Keys(dict);
}

/**
 * \brief Create a callable function for given signature
 *
 * \tparam Signature Signature of function to create
 */
template<class Signature, template<class> class DerivativeTraits = Dune::Functions::DefaultDerivativeTraits>
auto makeFunction(const Reference& f)
{
    using Range = typename Dune::Functions::SignatureTraits<Signature>::Range;
    using Domain = typename Dune::Functions::SignatureTraits<Signature>::Domain;
    return [callable = Python::Callable(f)](const Domain& x) {
        return callable(x).template toC<Range>();
    };
}

/**
 * \brief Create a callable differentiable function for given signature
 *
 * \tparam Signature Signature of function to create
 * \tparam DerivativeTraits Traits template to use for construction of derivative signatures
 * \tparam R Must derive from Python::Reference
 * \param f Callable python objects for the function and its derivatives
 */
template<class Signature, template<class> class DerivativeTraits = Dune::Functions::DefaultDerivativeTraits, class... R>
auto makeDifferentiableFunction(const R&... f)
{
    auto signatureTag = Dune::Functions::SignatureTag<Signature, DerivativeTraits>();
    auto derivativeSignatureTags = Dune::Functions::derivativeSignatureTags<sizeof...(f)-1>(signatureTag);

    return Dune::Std::apply([&f..., &signatureTag](auto... tag) {
            return Dune::Functions::makeDifferentiableFunctionFromCallables(signatureTag,
                makeFunction<typename decltype(tag)::Signature>(f)...);
        }, derivativeSignatureTags);
}

/**
 * \brief If a python error occured throw an exception and clear python error indicator
 *
 * \param origin A string describing the origin of the error
 */
void handlePythonError(const std::string& origin, const std::string& message)
{
    PyObject *rawType, *rawValue, *rawTraceback;
    PyErr_Fetch(&rawType, &rawValue, &rawTraceback);

    Reference type(rawType);
    Reference value(rawValue);
    Reference traceback(rawTraceback);
    if (type and value)
        DUNE_THROW(Dune::Exception,
            "Python error occured." << std::endl <<
            "  Origin:  " << origin << std::endl <<
            "  Error:   " << message << std::endl <<
            "  Python error message: " << value.str() );
    else if (type)
        DUNE_THROW(Dune::Exception,
            "Python error occured." << std::endl <<
            "  Origin:  " << origin << std::endl <<
            "  Error:   " << message);
}


} // end of namespace Python



#else
    #warning dunepython.hh was included but python was not found or enabled!
#endif // DUNE_FUFEM_PYTHON_COMMON_HH


#endif

